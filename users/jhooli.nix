{ config, pkgs, home-manager, ... }:

{
  imports = [ ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.jhooli = {
    description = "Jooa Hooli";
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDKN96qIkShceoSUT4nBAp2S/cpFaJA1oOzeCzwZ5VK4th2GjU72daEMFzGjSgwNW3QF3QaxSh/CZD+MMeBYSndpNQa6dYVAA/GKKUQ5cVVa/mRIV+OFCgLnyzzCxCDoBA5+uB1mXHiGfqAhTMtMsoTRPOVB6lvujNKr16JRwmAuGU6gs32ISmPOFGoCJgXh23G0m/vns9zig5qQRhEaA10yw+fxIFdhjjrgb0hPyQ31DlSGB2nw/BfdUzTmnmvIyj3fadOlpZdhjWu7QuxT8SvUUvvleEj1o8q6Slg4KuG/lIVojtkwlVNjWHtVwHnFxgTgfcdsYCkna62hOBWrvWtMrftqh/rOuz+I47TLQ9JcfiI3m9tFQS4ahDHnWD83zB13Jq+3filL1e6MwaYEytvUwJJlGREN4R1+iR9UhOz2p67mtxMx8pr1aMSFHwiTRq/d3HvdnBYxb8n5BYxWBSujs6opMK97uhiHEjyTjSjHO6gPncEFe/qIddNTt7fLk9TYrmDyVYJSCssU0gxn4MgDXYqT9FFBdJQuxG1QaG5cKeKfl21ijSeDOsniUADCPeT32DFouufVSpdOKdyvIQI+gURErauTQNnHm92ZXmUZKrE+PH3WNsNxjUlYS2mUzvrfo7GXmWCdhU+0qm9ors6jzRO0X8we7aQROR0CP8mCQ== cardno:24_038_760"
    ];
    isNormalUser = true;
    initialPassword = "1235";
    extraGroups = [ "wheel" "video" "networkmanager" "scanner" "lp"];
  };
}
