{ pkgs, ... }:
{
  environment.variables = {
    EDITOR = "vim";
  };
  environment.systemPackages = with pkgs; [
    ((vim_configurable.override { }).customize {
      name = "vim";
      vimrcConfig.packages.myplugins = with pkgs.vimPlugins; {
        start = [ vim-nix vim-lastplace ];
        opt = [ ];
      };
      vimrcConfig.customRC = ''
        "
        set nocompatible
        set backspace=indent,eol,start
        syntax on
        set shiftwidth=2
        set autoindent
        set smarttab
        set expandtab
        set number
        set relativenumber
      '';
    })
  ];
}
