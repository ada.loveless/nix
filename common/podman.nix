{ config, pkgs, ... }:

{
  virtualisation = {
    containers = {
      enable = true;
      containersConf.settings = {
        containers = {
          # As I mainly use podman interactively, I want to disable spamming
          # the syslog
          log_driver = "none";
        };
      };
    };
    podman = {
      enable = true;
      dockerCompat = true;
      autoPrune = {
        enable = true;
        dates = "weekly";
      };
      defaultNetwork.settings = {
        # Maybe this fixes our name resolution issues.
        dns_enabled = true;
      };
    };
  };

  # virtualisation.oci-containers.containers

  programs.zsh.shellAliases = {
    pm-run = "podman run --rm -it --volume \"$PWD:$PWD\" --workdir \"$PWD\"";
  };
}
