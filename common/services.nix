{ config, pkgs, ... }:

{
  services = {
    openssh = {
      enable = true;
      settings = {
        PasswordAuthentication = false;
        PermitRootLogin = "no";
        X11Forwarding = true;
        # LoginGraceTime = 0;
      };
    };
    pcscd.enable = true;
    power-profiles-daemon.enable = true;
    # TODO: figure out greetd on Xorg hosts
    avahi = {
      enable = true;
      nssmdns4 = true;
      nssmdns6 = false;
      openFirewall = true;
      publish = {
        enable = true;
        addresses = true;
        domain = true;
        hinfo = true;
        userServices = true;
      };
    };
  };
  systemd.services.greetd.serviceConfig = {
    Type = "idle";
    StandardInput = "tty";
    StandardOutput = "tty";
    StandardError = "journal";
    TTYReset = true;
    TTYHangup = true;
    TTYDisallocate = true;
  };
}
