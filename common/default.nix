{ config, lib, pkgs, home-manager, ... }:

{
  # Configuration and programs that should be present on all hosts
  imports = [
    ./services.nix
    ./vim.nix
    ./zsh.nix
    ./make.nix
    ./podman.nix
    ./kitty.nix
  ];

  nix = {
    settings = {
      experimental-features = [ "nix-command" "flakes" ];
      eval-cache = false; # Telling me that there is a cached build failure is most unhelpful
    };
    optimise = {
      dates = [ "06:00" ];
      automatic = true;
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 14d";
    };
  };
  nixpkgs.config.allowUnfree = true;

  services.guix = {
    enable = true;
    gc.enable = true;
  };

  documentation = {
    nixos.enable = true;
    info.enable = true;
  };

  security.polkit.enable = true;

  programs.git.lfs.enable = true;

  # Select internationalisation properties.
  i18n =
    let
      en = "en_GB.UTF-8";
      dk = "en_DK.UTF-8";
      de = "de_DE.UTF-8";
    in
    {
      # supportedLocales = [ en dk de ];
      defaultLocale = de;
      extraLocaleSettings = {
        LANG = en;
        LC_NUMERCI = en;
        LC_TIME = dk;
        LC_MONETARY = de;
        LC_PAPER = de;
      };
    };
  # i18n.defaultLocale = "en_IE.UTF-8";
  console = {
    earlySetup = true;
    font = "default8x16"; # I have a pair of fairly good eyes, thank you very much
    keyMap = "dvorak-uk";
    # TODO: remap caps to control?
  };

  security.sudo.extraRules = [
    {
      groups = [ "wheel" ];
      commands = [
        {
          command = "${config.system.path}/bin/dmesg -w -T";
          options = [ "NOPASSWD" ];
        }
      ];
    }
  ];

  nixpkgs.config.permittedInsecurePackages = [
    "olm-3.2.16"
  ];

  environment.systemPackages = with pkgs; [
    curl
    git
    git-lfs
    killall
    file
    htop
    tree
    bat
    fd
    ripgrep
    ffmpeg
    jq
    yq
    skim
    nmap
    pciutils
    usbutils
    nix-output-monitor
    nix-tree
    nix-index
    unzip
    man-pages
    doggo
    unzip
    gcc
    ncdu
    wireguard-tools
    imagemagick
    rnv-cli
  ];

  networking.firewall.enable = true;

  qt = {
    enable = true;
    platformTheme = "qt5ct";
  };

}
