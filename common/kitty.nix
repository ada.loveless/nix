{ config, lib, pkgs, ... }:

{
  boot.loader.grub = {
    gfxpayloadEfi = "keep";
    splashImage = ./cat.png;
    splashMode = "normal";
    theme = "${pkgs.libsForQt5.breeze-grub}/grub/themes/breeze";
  };
  boot.plymouth = {
    enable = true;
    font = "${pkgs.atkinson-hyperlegible}/share/fonts/opentype/AtkinsonHyperlegible-Regular.otf";
    logo = ./cat.png;
  };
}
