{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    gnumake
  ];
  environment.sessionVariables = {
    # - O=target groups output by target when running in parallel and avoids
    #    mangled output
    # - warn-undefined-variables warns when using undefined references
    MAKEFLAGS="";
    GNUMAKEFLAGS="w -Otarget --warn-undefined-variables";
  };
}
