{ config, lib, pkgs, ...}:

{
  system.autoUpgrade = {
    enable = true;
    flake = "git+https://gitlab.com/ada.loveless/nix.git";
    flags = [
      "-L"
    ];
    dates = "weekly";
    randomizedDelaySec = "1h";
  };
}
