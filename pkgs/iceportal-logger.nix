{ lib, rustPlatform, fetchFromGitLab }:

rustPlatform.buildRustPackage {
  pname = "iceportal-logger";
  version = "0.1.0";

  src = fetchFromGitLab {
    owner = "ada.loveless";
    repo = "iceportal";
    rev = "e67ee9f4eef136fe4cefd8f331da42782318c0ac";
    hash = "sha256-Nmq6KUz6b4w381CN3Viz5ciScKvNcaer2+TPEAPCg5E=";
  }; 

  cargoHash = "sha256-6/4vcJxxfm7KJgi98ehOzn9ykOKUyoDmF0G05++YEnA=";

  buildAndTestSubdir = "iceportal-logger";

  meta = with lib; {
    homepage = "https://gitlab.com/ada.loveless/iceportal";
    description = "A Logger for iceportal data";
    platforms = platforms.linux;
  };
}
