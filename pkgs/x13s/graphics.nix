{ stdenvNoCC, fetchurl, innoextract }:

stdenvNoCC.mkDerivation {
  pname = "x13s-graphics";
  version = "1.0.0";
  src = fetchurl {
    url = "https://download.lenovo.com/pccbbs/mobiles/n3hdr20w.exe";
    hash = "sha256-Jwyl9uKOnjpwfHd+VaGHjYs9x8cUuRdFCERuXqaJwEY=";
  };
  unpackCmd = "${innoextract}/bin/innoextract -s $curSrc";

  installPhase = ''
    runHook preInstall
    mkdir -vp "$out/lib/firmware/qcom/sc8280xp/LENOVO/21BX"
    cp -v */*.mbn "$out/lib/firmware/qcom/sc8280xp/LENOVO/21BX/"
    runHook postInstall
  '';
}
