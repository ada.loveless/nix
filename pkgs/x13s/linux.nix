{ buildLinux, fetchFromGitHub, lib, ... }@args:

buildLinux {
  src = fetchFromGitHub {
    owner = "jhovold";
    repo = "linux";
    rev = "699d5b0ccdc1a343ded65aa876f0b720ca7dada9";
    hash = "sha256-QjStPt3c/v55lJUgn4Rdx/WTaQKdUxNL9nwkIKQ8O2Q=";
  };
  version = "6.12.0-rc5";
  defconfig = "johan_defconfig";
  kernelPatches = (args.kernelPatches or [ ]) ++ [
    {
      # This patch is probably not necessary to boot the system, but it is
      # necessary for the initrd build that otherwise fails since it can't
      # find the uhci-hcd modules.
      name = "enable usb";
      patch = null;
      extraStructuredConfig = with lib.kernel; {
        USB_UHCI_HCD = module;
        USB_EHCI_PCI = module;
        USB_PCI = yes;
        USB_OHCI_HCD_PCI = module;
        USB_XHCI_PCI = module;
      };
    }
  ];
}
