{ lib, rustPlatform, fetchFromGitLab }:

rustPlatform.buildRustPackage {
  pname = "travelynx-status";
  version = "0.1.0-1";

  src = fetchFromGitLab {
    owner = "ada.loveless";
    repo = "travelynx-rs";
    rev = "ea31bee19a3d0a39119c6b8c5875cf496f0eaab5";
    hash = "sha256-xyld4BJsGdmAiSKqF4zn76jytK/ciT0cZ9/hVTTERuE=";
  }; 

  cargoHash = "sha256-NwklH0+qyExSRyAlFiJeR1EwdWF4LkoRBMM2kfaitY8=";

  meta = with lib; {
    homepage = "https://gitlab.com/ada.loveless/travelynx-rs";
    description = "A Travelynx status monitor for waybar";
    platforms = platforms.linux;
  };
}
