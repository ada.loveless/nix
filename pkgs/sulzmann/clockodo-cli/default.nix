{ rustPlatform, fetchFromGitLab, openssl, pkg-config }:

rustPlatform.buildRustPackage {
  pname = "clockodo-cli";
  version = "0.1.0";
  nativeBuildInputs = [
    openssl
    pkg-config
  ];
  PKG_CONFIG_PATH = "${openssl.dev}/lib/pkgconfig";
  src = fetchFromGitLab {
    domain = "gitlab.sulzmann.energy";
    owner = "ahartmann";
    repo = "clockodo-cli";
    rev = "e099b87102af8fc7d3f03e149a9feca97b89a5f8";
    hash = "sha256-lrYK/NJ8C+3TFYkYUbgzV5WbBIQyhvxW0CQ5/Jyi+/w=";
  };
  buildAndTestSubdir = "clockodo-cli";
  cargoHash = "sha256-6NcyDzYshLQZex5ponnYPCD1gNDbvDXxy0jQOtkkIUY=";
}
