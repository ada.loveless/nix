{ rustPlatform, fetchFromGitLab }:

rustPlatform.buildRustPackage {
  pname = "typst-lsp";
  version = "0.13.1-sulzmann";
  src = fetchFromGitLab {
    domain = "gitlab.sulzmann.energy";
    owner = "work/typst";
    repo = "typst-lsp";
    rev = "0.13.1";
    hash = "sha256-SwDlenE+UhqstF8JP6pL1f3eIyb5Xgjb+qnZ12/yRf4=";
  };
  cargoLock = {
    lockFile = ./Cargo.lock;
    allowBuiltinFetchGit = true;
  };
  postPatch = ''
    rm Cargo.lock
    ln -s ${./Cargo.lock} Cargo.lock
  '';
  doCheck = false;
}
