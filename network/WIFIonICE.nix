{ config, pkgs, home-manager, ... }:

{
  systemd.services = {
    iceportal-logger = {
      description = "A logger for iceportal data";
      serviceConfig = {
        Type = "simple";
        User = "ada";
        Group = "users";
        ExecStart = "${pkgs.iceportal-logger}/bin/iceportal-logger --base-path /home/ada/Data/ICE/";
      };
    };
  };

  networking.networkmanager.ensureProfiles.profiles = {
    WIFIonICE = {
      connection = {
        id = "WIFIonICE";
        type = "wifi";
      };
      wifi = {
        mode = "infrastructure";
        ssid = "WIFIonICE";
      };
      ipv4 = {
        method = "auto";
      };
      ipv6 = {
        addr-gen-mode = "stable-privacy";
        method = "auto";
      };
    };
  };

  networking.networkmanager.dispatcherScripts = [
    {
      #!/usr/bin/env bash
      source = pkgs.writeText "connect_ice" ''
        set -euxo pipefail
        # Run if ssid is WIFIonICE
        # Run if state is up
        ACTION="$2"
        if [[ "$ACTION" == "up" ]]
        then
          if [[ "$CONNECTION_ID" == "WIFIonICE" ]]
          then
            ${pkgs.systemd}/bin/systemctl start iceportal-logger
            ${pkgs.curl}/bin/curl 'https://login.wifionice.de/cna/logon' -sSL -X POST
          fi
        fi
        if [[ "$ACTION" == "down" ]]
        then
          if [[ "$CONNECTION_ID" == "WIFIonICE" ]]
          then
            ${pkgs.systemd}/bin/systemctl stop iceportal-logger
          fi
        fi
      '';
      type = "basic";
    }
  ];

}
