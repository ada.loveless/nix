{ config, pkgs, ... }:

{
  networking.networkmanager.dispatcherScripts = [
    {
      source = pkgs.writeText "disable_wifi" ''
        set -euxo pipefail
        IFACE="$1"
        ACTION="$2"
        if [[ "$IFACE" =~ ^(eth|en).*$ ]]
        then 
          case "$ACTION" in
            up)
              ${pkgs.networkmanager}/bin/nmcli radio wifi off
            ;;
            down)
              ${pkgs.networkmanager}/bin/nmcli radio wifi on
            ;;
          esac
        fi
      '';
      type = "basic";
    }
  ];
}
