{ config, pkgs, ... }:

{
  networking.networkmanager.plugins = with pkgs; [
    networkmanager-openconnect
  ];
  networking.networkmanager.ensureProfiles.profiles = {
    DKFZ-VPN = {
      connection = {
        id = "DKFZ-VPN";
        type = "vpn";
        autoconnect = "false";
      };
      vpn = {
        service-type = "org.freedesktop.NetworkManager.openconnect";
        protocol = "anyconnect";
        autoconnect-flags = 0;
        certsigs-flags = 0;
        cookie-flags = 2;
        disable_udp = "no";
        enable_csd_trojan = "no";
        gateway = "https://gate.dkfz-heidelberg.de/";
        gateway-flags = 2;
        gwcert-flags = 2;
        lasthost-flags = 0;
        pem_passphrase_fsid = "no";
        prevent_invalid_cert = "no";
        reported_os = "win";
        stoken_source = "disabled";
        useragent = "AnyConnect Windows 4.10.06079";
        xmlconfig-flags = 0;
        authtype = "password";
      };
      vpn-secrets = {
        "form:main:username" = "j586i";
        lasthost = "https://gate.dkfz-heidelberg.de";
      };
      ipv4 = {
        method = "auto";
      };
      ipv6 = {
        addr-gen-mode = "stable-privacy";
        method = "auto";
      };
    };
  };
}
