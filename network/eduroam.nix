{ config, pkgs, ... }:

{
  networking.networkmanager.ensureProfiles.profiles = {
    eduroam = {
      connection = {
        id = "eduroam";
        type = "wifi";
      };
      wifi = {
        mode = "infrastructure";
        ssid = "eduroam";
      };
      wifi-security = {
        auth-alg = "open";
        key-mgmt = "wpa-eap";
      };
      "802-1x" = {
        anonymous_identyt = "eduroamHDcat2024@uni-heidelberg.de";
        ca-cert = "/home/ada/.ca.pem";
        eap = "ttls";
        password-flags = "1";
        identity = "ky182@uni-heidelberg.de";
        phase2-auth = "mschapv2";
      };
      ipv4 = {
        method = "auto";
      };
      ipv6 = {
        addr-gen-mode = "stable-privacy";
        method = "auto";
      };
    };
  };
}
