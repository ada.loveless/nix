{ config, lib, pkgs, ... }:
with lib;
let cfg = config.hardware.x13s;
in
{
  # Config to be added here
  options = {
    hardware.x13s.enable = mkEnableOption "Enable configuration for Thinkpad x13s specific quirks";
  };
  config = mkIf cfg.enable {
    boot = {
      loader.grub = {
        extraConfig = ''
          terminal_input console
          terminal_output gfxterm
        '';
      };
      supportedFilesystems = pkgs.lib.mkForce [ "btrfs" "reiserfs" "vfat" "f2fs" "xfs" "ntfs" "cifs" ];
      kernelParams = [
        "arm64.nopauth"
        "clk_ignore_unused"
        "pd_ignore_unused"
        "iommu.passthrough=0"
        "iommu.strict=0"
        "efi=noruntime"
        "pcie_aspm.policy=powersupersave"
        "modprobe.blacklist=qcom_q6v5_pas"
        "rd.driver.blacklist=msm"
        "boot.debug1devices"
        # "devicetree=sc8280xp-lenovo-thinkpad-x13s.dtb"
      ];
      initrd = {
        includeDefaultModules = true;
        availableKernelModules = [
        # Needed according to a kernel fork
        # https://github.com/jhovold/linux/commit/ee2ccfe6dccbd8e7599e23c2992d471c4e3da58d
          "nvme"
          "phy_qcom_qmp_pcie"
          "pcie_qcom"
          "phy_qcom_qmp_ufs"
          "ufs_qcom"
          "i2c_hid_of"
          "i2c_qcom_geni"
          "leds_qcom_lpg"
          "pwm_bl"
          "qrtr"
          "pmic_glink_altmode"
          "gbio_sbu_mux"
          "phy_qcom_qmp_combo"
          "gpucc_sc8280xp"
          "dispcc_sc8280xp"
          "phy_qcom_edp"
          "panel_edp"
          "msm"
          # # For USB storage
          # # For Stage 1 keyboard
          # "qcom_pm8008"
          # "regmap_i2c"
          # # Other things
          # # "qrtr"
          # "pmic_glink_altmode"
          # "gpio_sbu_mux"
          # "panel-edp"
          # # Debian thinks these are necessary
          # "leds_qcom_lpg"
          # "qnoc-sc8280xp"
          # # In archiso-x13s
          # "phy_qcom_qmp_pcie"
          # "phy_qcom_edp"
          # # "i2c_hid_of"
          # # Probably necessary for input
          # "i2c_qcom_cci"
          # # In arch mkinitcpio
          # "pcie_qcom"
          # "i2c_qcom_lpg"
          # "pmic_glink_altmode"
          # "gbio_sbu_mux"
          # "phy_qcom_qmp_combo"
          # "panel-edp"
          # "msm"
          # # Maybe useful
          # "cdrom"
          # "loop"
          # "overlay"
        ];
      };
    };
    #isoImage = {
    #  makeBiosBootable = false;
    #  makeEfiBootable = true;
    #  makeUsbBootable = true;
    #  # edition = "custom-x13s";
    #  contents = [
    #    {
    #      source = "${config.boot.kernelPackages.kernel}/dtbs/qcom/sc8280xp-lenovo-thinkpad-x13s.dtb";
    #      target = "/boot/sc8280xp-lenovo-thinkpad-x13s.dtb";
    #    }
    #  ];
    #};
  };
}
