{ config, pkgs, ... }:
{
  imports = [
    ./services
    ./proxy.nix
  ];
}
