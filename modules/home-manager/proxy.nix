{ lib, config, pkgs, ... }:
with lib;
let
  cfg = config.proxy.ssh;
in
{
  options.proxy.ssh = {
    enable = mkEnableOption "SSH Proxy Setup";
    hosts = mkOption {
      description = "SSH Hosts to proxy";
      example = [ "gitlab.com" "github.com" ];
      default = [ ];
      type = types.listOf types.str;
    };
    proxy.host = mkOption {
      description = "Proxy hostname to connect to";
      type = types.str;
      example = "example.org";
    };
    proxy.port = mkOption {
      description = "The proxy's port to connect to";
      type = types.int;
      default = 80;
    };
    proxyCommand = mkOption {
      description = "Command to proxy SSH with";
      type = types.str;
      default = "${pkgs.corkscrew}/bin/corkscrew ${cfg.proxy.host} ${builtins.toString cfg.proxy.port} %h %p";
    };
  };
  config = mkIf cfg.enable {
    programs.ssh = {
      enable = true;
      matchBlocks = builtins.listToAttrs (
        map
          (host: {
            name = host;
            value = { proxyCommand = cfg.proxyCommand; };
          })
          cfg.hosts
      );
    };
  };
}
