# TODOs:
# - move work to their gitlab
# - fix tvarelynx secrets
# - autorestart applet daemons
{
  description = "Computer configuration configurer";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-hardware = {
      url = "github:NixOS/nixos-hardware/master";
    };
    typst-sulzmann = {
      type = "gitlab";
      owner = "work%2Ftypst";
      repo = "typst";
      host = "gitlab.sulzmann.energy";
      ref = "0.13.0";
    };
    signal-desktop = {
      type = "gitlab";
      owner = "cyberchaoscreatures";
      repo = "nixlib";
      host = "cyberchaos.dev";
    };
    rnv-cli = {
      type = "gitlab";
      owner = "ada.loveless";
      repo = "rnv";
    };
  };

  outputs = { self, nixpkgs, nixpkgs-unstable, home-manager, disko, nixos-hardware, ... } @ inputs:
    let
      inherit (self) outputs;
      overlay-unstable = self: super: {
        unstable = nixpkgs-unstable.legacyPackages.${super.system};
      };
      overlay-extra = self: super: {
        x13s-firmware = super.callPackage ./pkgs/x13s/firmware.nix { };
        x13s-graphics = super.callPackage ./pkgs/x13s/graphics.nix { };
        x13s-linux = super.callPackage ./pkgs/x13s/linux.nix { };
        travelynx = super.callPackage ./pkgs/travelynx.nix { };
        # rnv-cli = super.callPackage ./pkgs/rnv-cli.nix { };
        rnv-cli = inputs.rnv-cli.packages.${super.system}.rnv-cli;
        iceportal-logger = super.callPackage ./pkgs/iceportal-logger.nix { };
        google-fonts = super.google-fonts.overrideAttrs {
          version = "unstable-2025-02-27";
          src = super.fetchFromGitHub {
            owner = "google";
            repo = "fonts";
            rev = "0e8e894810ca309d805f62011d90187868f5c6da";
            hash = "sha256-DViqAzYGDKrq+KiGBf4uAivB4j2nbX6dxak7Z/m/B34=";
          };
        };
      };
      overlays-default = [ overlay-unstable overlay-extra inputs.rnv-cli.overlays.default ];
      overlay-signal-aarch64 = self: super: {
        signal-desktop = inputs.signal-desktop.legacyPackages.aarch64-linux.signal-desktop;
      };
      overlay-sulzmann = self: super: {
        typst-dev = inputs.typst-sulzmann.packages.${super.system}.typst-dev;
        typst-lsp = super.callPackage ./pkgs/sulzmann/typst-lsp/default.nix { };
        clockodo-cli = super.callPackage ./pkgs/sulzmann/clockodo-cli/default.nix { };
      };
    in
    {
      homeManagerModules = import ./modules/home-manager;
      nixosModules = import ./modules/nixos;

      pkgs = let pkgs = nixpkgs; in import ./pkgs { inherit pkgs; };

      nixosConfigurations = {

        iso-x86_64 = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            "${nixpkgs}/nixos/modules/installer/cd-dvd/installation-cd-minimal-new-kernel-no-zfs.nix"
            "${nixpkgs}/nixos/modules/installer/cd-dvd/channel.nix"
            outputs.nixosModules
            ({ config, pkgs, ... }: { nixpkgs.overlays = overlays-default; })
            ./graphical
            ./users/nixos.nix
            ./hosts/iso-x86_64/configuration.nix
            home-manager.nixosModules.home-manager
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.nixos = import homes/nixos/default.nix;
              home-manager.extraSpecialArgs = { inherit inputs; };
              home-manager.sharedModules = [
                homes/graphical
                homes/graphical/sway/minimal.nix
                outputs.homeManagerModules
              ];
            }
          ];
        };

        iso-aarch64 = nixpkgs.lib.nixosSystem {
          system = "aarch64-linux";
          modules = [
            "${nixpkgs}/nixos/modules/installer/cd-dvd/installation-cd-minimal-new-kernel-no-zfs.nix"
            "${nixpkgs}/nixos/modules/installer/cd-dvd/channel.nix"
            outputs.nixosModules
            ({ config, pkgs, ... }: { nixpkgs.overlays = overlays-default; })
            ./common
            ./users/nixos.nix
            ./hosts/iso-aarch64/configuration.nix
            {
              nixpkgs = {
                config.allowUnsupportedSystem = true;
                hostPlatform.system = "aarch64-linux";
                buildPlatform.system = "x86_64-linux";
              };
            }
            home-manager.nixosModules.home-manager
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.nixos = import homes/nixos/minimal.nix;
              home-manager.extraSpecialArgs = { inherit inputs; };
            }
          ];
        };

        ada-nb01 = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            outputs.nixosModules
            ({ config, pkgs, ... }: { nixpkgs.overlays = overlays-default; })
            ./graphical
            ./common/upgrades.nix
            ./users/ada.nix
            ./hosts/ada-nb01/configuration.nix
            ./printers/default.nix
            ./printers/g550.nix
            nixos-hardware.nixosModules.lenovo-thinkpad-x220
            home-manager.nixosModules.home-manager
            {
              home-manager.backupFileExtension = "backup";
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.ada = import homes/ada/graphical.nix;
              home-manager.extraSpecialArgs = { inherit inputs; };
              home-manager.sharedModules = [
                outputs.homeManagerModules
                homes/graphical
                homes/graphical/sway/laptop.nix
              ];
            }
          ];
        };

        ada-nb02 = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          specialArgs = { inherit inputs outputs; };
          modules = [
            outputs.nixosModules
            ({ config, pkgs, ... }: { nixpkgs.overlays = overlays-default; })
            ./graphical
            ./common/upgrades.nix
            ./users/ada.nix
            ./hosts/ada-nb02/configuration.nix
            ./printers/default.nix
            ./printers/g550.nix
            nixos-hardware.nixosModules.common-cpu-intel-kaby-lake
            nixos-hardware.nixosModules.common-pc-laptop
            nixos-hardware.nixosModules.common-pc-ssd
            home-manager.nixosModules.home-manager
            {
              home-manager.backupFileExtension = "backup";
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.ada = import homes/ada/graphical.nix;
              home-manager.extraSpecialArgs = { inherit inputs; };
              home-manager.sharedModules = [
                outputs.homeManagerModules
                homes/graphical
                homes/graphical/sway/laptop.nix
              ];
            }
          ];
        };

        tabby = nixpkgs.lib.nixosSystem {
          system = "aarch64-linux";
          specialArgs = { inherit inputs outputs; };
          modules = [
            outputs.nixosModules
            { nixpkgs.overlays = overlays-default ++ [ overlay-signal-aarch64 ]; }
            ./graphical
            ./users/ada.nix
            ./users/jooa.nix
            ./hosts/tabby/configuration.nix
            ./printers/default.nix
            ./printers/rzl.nix
            ./network/WIFIonICE.nix
            ./network/eduroam.nix
            { nixpkgs.overlays = [ (self: super: { compressFirmwareZstd = nixpkgs.lib.id; }) ]; }
            home-manager.nixosModules.home-manager
            {
              home-manager.backupFileExtension = "backup";
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.ada = import homes/ada/graphical.nix;
              home-manager.users.jooa = import homes/jooa/graphical.nix;
              home-manager.extraSpecialArgs = { inherit inputs; };
              home-manager.sharedModules = [
                outputs.homeManagerModules
                homes/graphical
                homes/graphical/sway/laptop.nix
                homes/hosts/tabby.nix
              ];
            }
          ];
        };

        ada-pc01 = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            outputs.nixosModules
            { nixpkgs.overlays = overlays-default ++ [ overlay-sulzmann ]; }
            ./graphical
            ./users/ada.nix
            ./users/jooa.nix
            ./users/jhooli.nix
            ./hosts/ada-pc01/configuration.nix
            ./printers/default.nix
            ./printers/g550.nix
            nixos-hardware.nixosModules.common-cpu-amd
            nixos-hardware.nixosModules.common-gpu-amd
            home-manager.nixosModules.home-manager
            {
              home-manager.backupFileExtension = "backup";
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.ada = import homes/ada/graphical.nix;
              home-manager.users.jooa = import homes/jooa/graphical.nix;
              home-manager.users.jhooli = import homes/jhooli/graphical.nix;
              home-manager.extraSpecialArgs = { inherit inputs; };
              home-manager.sharedModules = [
                outputs.homeManagerModules
                homes/hosts/ada-pc01.nix
                homes/graphical
                homes/graphical/sway/desktop.nix
                homes/common/mpd.nix
              ];
            }
          ];
        };

        jhooli-nb01 = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          specialArgs = { inherit inputs outputs; };
          modules = [
            outputs.nixosModules
            { nixpkgs.overlays = overlays-default ++ [ overlay-sulzmann ]; }
            ./common
            ./graphical
            ./users/jhooli.nix
            ./users/ada.nix
            ./hosts/jhooli-nb01/configuration.nix
            ./network/work-disable-wifi-on-eth.nix
            ./printers/default.nix
            ./printers/work.nix
            ./printers/g550.nix
            nixos-hardware.nixosModules.common-pc-laptop
            nixos-hardware.nixosModules.common-pc-ssd
            nixos-hardware.nixosModules.lenovo-thinkpad-t14s-amd-gen4
            home-manager.nixosModules.home-manager
            {
              home-manager.backupFileExtension = "backup";
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.jhooli = import homes/jhooli/graphical.nix;
              home-manager.users.ada = import homes/ada/graphical.nix;
              home-manager.extraSpecialArgs = { inherit inputs; };
              home-manager.sharedModules = [
                outputs.homeManagerModules
                homes/hosts/jhooli-nb01.nix
                homes/graphical
                homes/graphical/sway/laptop.nix
              ];
            }
          ];
        };

      };
    };
}
