# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, lib, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
  ];
  boot.loader.grub = {
    enable = true;
    efiSupport = true;
    device = "nodev";
    useOSProber = true;
  };
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "ada-pc01";
  time.timeZone = "Europe/Berlin";

  security.polkit.enable = true;
  networking.firewall.enable = true;

  hardware.sane = {
    enable = true;
    extraBackends = [
      pkgs.epkowa
    ];
  };
  environment.systemPackages = with pkgs; [
    xsane
    simple-scan
    libsForQt5.skanlite
    ausweisapp
  ];

  services.resolved.enable = true;

  networking.firewall.allowedTCPPorts = [ 
    24727 # Ausweisapp
  ];
  networking.firewall.allowedUDPPorts = [ 
    24727 # Ausweisapp
  ];

  programs.steam.enable = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?

}

