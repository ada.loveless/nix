{ pkgs, libs, ... }:

{

  # nixpkgs = {
    # hostPlatform = pkgs.lib.mkDefault "x86_64-linux";
    # config.allowUnfree = true;
  # };

  services = {
    qemuGuest.enable = true;
    openssh.settings.PermitRootLogin = pkgs.lib.mkForce "yes";
  };

  boot = {
    # kernelPackages = pkgs.linuxPackages_latest;
    supportedFilesystems = pkgs.lib.mkForce [ "btrfs" "vfat" "f2fs" "xfs" "ntfs" ];
  };

  networking = {
    hostName = "nixiso-x86_64";
    wireless.enable = false;
  };
  time.timeZone = "Europe/Berlin";

  hardware.graphics.enable = true;
  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  services.greetd.settings.initial_session = {
    command = "sway";
    user = "nixos";
  };

  systemd = {
    services.sshd.wantedBy = pkgs.lib.mkForce [ "multi-user.target" ];
    targets = {
      sleep.enable = false;
      suspend.enable = false;
      hibernate.enable = false;
      hybrid-sleep.enable = false;
    };
  };

  users.extraUsers.root.password = "nixos";

}
