{ pkgs, lib, config, ... }:

{

  # nixpkgs = {
  # hostPlatform = pkgs.lib.mkDefault "x86_64-linux";
  # config.allowUnfree = true;
  # };

  services = {
    qemuGuest.enable = true;
    openssh.settings.PermitRootLogin = pkgs.lib.mkForce "yes";
  };

  networking = {
    hostName = "nixiso-aarch64";
    wireless.enable = false;
  };
  time.timeZone = "Europe/Berlin";

  # hardware.x13s.enable = true;

  # hardware.opengl.enable = true;
  # hardware.bluetooth.enable = true;
  # services.blueman.enable = true;

  services.greetd.settings.initial_session = {
    command = "sway";
    user = "nixos";
  };

  systemd = {
    # services.sshd.wantedBy = pkgs.lib.mkForce [ "multi-user.target" ];
    targets = {
      sleep.enable = false;
      suspend.enable = false;
      hibernate.enable = false;
      hybrid-sleep.enable = false;
    };
  };

  users.extraUsers.root.password = "nixos";

  isoImage = {
    makeBiosBootable = false;
    makeEfiBootable = true;
    makeUsbBootable = true;
    # edition = "custom-x13s";
    contents = [
      {
        source = "${config.boot.kernelPackages.kernel}/dtbs/qcom/sc8280xp-lenovo-thinkpad-x13s.dtb";
        target = "/boot/sc8280xp-lenovo-thinkpad-x13s.dtb";
      }
    ];
  };

  boot = {
    loader.grub = {
      extraConfig = ''
        terminal_input console
        terminal_output gfxterm
      '';
    };
    supportedFilesystems = pkgs.lib.mkForce [ "btrfs" "reiserfs" "vfat" "f2fs" "xfs" "ntfs" "cifs" ];
    # kernelPackages = let baseKernel = pkgs.linux_latest; in pkgs.linuxManualConfig {
    #   inherit (baseKernel) src modDirVersion;
    #   version = "${baseKernel.version}-x13s";
    #   configfile = ./johan_defconfig;
    #   allowImportFromDerivation = true;
    # };
    kernelPatches = [
      {
        name = "enable pcie_qcom";
        patch = null;
        extraStructuredConfig = with lib.kernel; {
          PCIE_QCOM = yes;
          ARCH_QCOM = yes;
          PCI = yes;
          PCI_MSI_IRQ_DOMAIN = yes;
          OF = yes;
        };
      }
      {
        name = "enable arm_mhu";
        patch = null;
        extraStructuredConfig = with lib.kernel; {
          ARM_MHU = yes;
          PLATFORM_MHU = yes;
        };
      }
      {
        name = "enable other things";
        patch = null;
        extraStructuredConfig = with lib.kernel; {
          QCOM_Q6V5_ADSP = module;
          NVMEM_QCOM_QFPROM = yes;
          QCOM_SSC_BLOCK_BUS = yes;
          PINCTRL_QCOM_SSBI_PMIC = yes;
          PINCTRL_LPASS_LPI = yes;
          PINCTRL_SC8280XP_LPASS_LPI = yes;
          # TYPEC_MUX_GPIO_SBU = yes;
          PHY_QCOM_QMP = yes;
          # PHY_QCOM_QMP_COMBO = yes;
          # PHY_QCOM_QMP_PCIE = yes;
          PHY_QCOM_QMP_UFS = yes;
          PHY_QCOM_QMP_USB = yes;
          PHY_QCOM_USB_SNPS_FEMTO_V2 = yes;
        };
      }
    ];
    # kernelPackages = pkgs.linux_latest;
    kernelParams = [
      "arm64.nopauth"
      "clk_ignore_unused"
      "pd_ignore_unused"
      # "iommu.passthrough=0"
      # "iommu.strict=0"
      "efi=noruntime"
      "pcie_aspm.policy=powersupersave"
      # "boot.debug1devices"
      # "devicetree=sc8280xp-lenovo-thinkpad-x13s.dtb"
    ];
    initrd = {
      includeDefaultModules = true;
      availableKernelModules = [
        # Needed according to a kernel fork
        # https://github.com/jhovold/linux/commit/ee2ccfe6dccbd8e7599e23c2992d471c4e3da58d
        "nvme"
        "phy_qcom_qmp_usb"
        "phy_qcom_qmp_combo"
        "phy_qcom_qmp_pcie"
        #"pcie_qcom_ep" # Is this needed?!?
        # should be included in the kernel with the above patch
        # "pcie_qcom"
        "phy_qcom_qmp_ufs"
        "ufs_qcom"
        "i2c_hid_of"
        "i2c_qcom_geni"
        "leds_qcom_lpg"
        "pwm_bl"
        "qrtr"
        "pmic_glink_altmode"
        "gpio_sbu_mux"
        "phy_qcom_qmp_combo"
        "gpucc_sc8280xp"
        "dispcc_sc8280xp"
        "phy_qcom_edp"
        "panel_edp"
        "msm"
      ];
    };
  };

}
