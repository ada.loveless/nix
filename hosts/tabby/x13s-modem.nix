{ config, lib, pkgs, ... }:

{

  networking.networkmanager.fccUnlockScripts = [
    {
      id = "105b:e0c3";
      path = "${pkgs.modemmanager}/share/ModemManager/fcc-unlock.available.d/105b";
    }
  ];

  systemd.services.ModemManager = {
    enable = true;
    path = [ pkgs.libqmi ]; # required by fcc-unlock-script of 105b:e0ab
    wantedBy = [ "multi-user.target" "network.target" ];
  };

  environment.systemPackages = with pkgs; [
    modemmanager
    modem-manager-gui
    libqmi
  ];

}
