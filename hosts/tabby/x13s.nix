{ config, lib, pkgs, ... }:

let
  dtb_path = "sc8280xp-lenovo-thinkpad-x13s.dtb";
in
{
  # Extra firmware packages
  hardware.firmware = [
    pkgs.x13s-firmware
    pkgs.x13s-graphics
  ];

  hardware.alsa.enablePersistence = false;

  hardware.deviceTree = {
    enable = true;
    filter = "sc8280xp-lenovo-thinkpad-x13s.dtb";
    name = "sc8280xp-lenovo-thinkpad-x13s.dtb";
  };

  boot = {
    # Forcibly ignore zfs I think?
    supportedFilesystems = pkgs.lib.mkForce [ "btrfs" "reiserfs" "vfat" "f2fs" "xfs" "ntfs" "cifs" ];

    # Kernel things. A new kernel is required for most features.
    kernelPackages = pkgs.linuxPackages_latest;
    # These options are more or less necessary at the moment
    kernelParams = [
      "arm64.nopauth" # Pointer authentication or something
      "clk_ignore_unused" # Ignores some extra clocks, saves power I think
      "pd_ignore_unused"
      # "efi=noruntime" # Not needed anymore with recent firmware
      "pcie_aspm.policy=powersupersave"
    ];

    # Boot loader
    loader.grub = {
      extraConfig = ''
        terminal_input console
        terminal_output gfxterm
      '';
      extraPerEntryConfig = ''
        devicetree /${dtb_path}
      '';
      extraFiles = {
        "${dtb_path}" = "${config.boot.kernelPackages.kernel}/dtbs/qcom/${dtb_path}";
      };
    };

    # Early boot
    initrd = {
      # These kernel modules are necessary for a variety of reasons
      availableKernelModules = [
        # Required for boot
        "nvme"
        "phy_qcom_qmp_pcie"
        #"pcie_qcom" # baked-into kernel
        # Required for keyboard
        "i2c_hid_of"
        "i2c_qcom_geni"
        # Required for more than 30s of display
        "leds_qcom_lpg"
        "pwm_bl"
        "qrtr"
        "pmic_glink_altmode"
        "gpio_sbu_mux"
        "phy_qcom_qmp_combo"
        "gpucc_sc8280xp"
        "dispcc_sc8280xp"
        "phy_qcom_edp"
        "panel_edp"
        "msm"
        # Required for unknown reasons (do I even need these??)
        "phy_qcom_qmp_usb" # Possibly when booting from an USB keys
        "phy_qcom_qmp_ufs" # Probably unnecessary?
        "ufs_qcom" # May be unnecessary
      ];
      # Make graphics firmware available in early boot
      systemd = {
        enable = true;
        emergencyAccess = true;
        contents =
          let
            modules = pkgs.makeModulesClosure {
              rootModules = config.boot.initrd.availableKernelModules ++ config.boot.initrd.kernelModules;
              kernel = config.system.modulesTree;
              firmware = config.hardware.firmware;
              allowMissing = false;
            };
            modulesWithExtra = pkgs.symlinkJoin {
              name = "modules-closure";
              paths = [
                modules
                pkgs.x13s-graphics
              ];
            };
          in
          {
            "/lib".source = lib.mkForce "${modulesWithExtra}/lib";
          };
      };
    };
  };

  powerManagement.cpuFreqGovernor = "ondemand";

  # User-space utilities that are useful for proper functioning of stuff.
  systemd.services = {
    # This is necessary to set the mac address for the bluetooth device
    bluetooth-x13s-mac = {
      wantedBy = [ "multi-user.target" ];
      before = [ "bluetooth.service" ];
      requiredBy = [ "bluetooth.service" ];

      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
        ExecStart = "${pkgs.util-linux}/bin/script -q -c '${pkgs.bluez}/bin/btmgmt --index 0 public-addr F4:A8:0D:12:F2:42'";
      };
    };
  };

  # May turn out to be important for actually functioning audio.
  environment.systemPackages = with pkgs; [
    alsa-ucm-conf
    alsa-utils
    alsa-lib
    libcamera
  ];

  # TODO:
  # - fix audio
  # - fix camera
  # - determine what to do about firmware (i.e. download from lenovo?)

}
