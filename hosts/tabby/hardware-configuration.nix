# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.luks.devices."luks".device = "/dev/disk/by-uuid/1c28bdc7-cc6c-4b39-a751-d2bb2d3b2f00";

  fileSystems."/" =
    { device = "/dev/mapper/luks";
      fsType = "btrfs";
      options = [ "subvol=@nix-root" ];
    };


  fileSystems."/nix" =
    { device = "/dev/mapper/luks";
      fsType = "btrfs";
      options = [ "subvol=@nix-nix" "noatime" ];
    };

  fileSystems."/home" =
    { device = "/dev/mapper/luks";
      fsType = "btrfs";
      options = [ "subvol=@nix-home" "compress=zstd" ];
    };

   fileSystems."/swap" = {
     device = "/dev/mapper/luks";
     fsType = "btrfs";
     options = [ "subvol=@swap" "noatime" ];
   };
   swapDevices = [{ device = "/swap/swapfile"; }];

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/6815-CA45";
      fsType = "vfat";
    };


  hardware.graphics.enable = true;
  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.enu1u3c2.useDHCP = lib.mkDefault true;
  # networking.interfaces.wlP6p1s0.useDHCP = lib.mkDefault true;
  # networking.interfaces.wwan0.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "aarch64-linux";
}
