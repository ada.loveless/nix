{ config, pkgs, ... }:

{
  programs.firefox = {
    enable = true;
    policies = {
      # See https://mozilla.github.io/policy-templates/
      Preferences = {
        "browser.tabs.inTitlebar" = 0;
        "browser.compactmode.show" = true;
        "browser.uidensity" = 1;
        "signon.rememberSignons" = false;
        "browser.translations.automaticallyPopup" = false;
        "browser.urlbar.showSearchSuggestionsFirst" = false;
      };
      DisableTelemetry = true;
      DisableFirefoxStudies = true;
      DisablePocket = true;
      DisableFirefoxAccounts = true;
      DisableAccounts = true;
      DisableProfileImport = true;
      OverrideFirstRunPage = "";
      OverridePostUpdatePage = "";
      DontCheckDefaultBrowser = true;
      DisplayBookmarksToolbar = "newtab";
      ManualAppUpdateOnly = true;
      OfferToSaveLogins = false;
      PasswordManagerEnabled = false;
      DownloadDirectory = "\${home}/Downloads";
      EnableTrackingProtection = {
        Value = true;
        Cryptomining = true;
        Fingerprinting = true;
      };
      ExtensionSettings = {
        # "*".installation_mode = "blocked"; # blocks all addons except the ones specified below
        # uBlock Origin:
        "uBlock0@raymondhill.net" = {
          install_url = "https://addons.mozilla.org/firefox/downloads/latest/ublock-origin/latest.xpi";
          installation_mode = "force_installed";
        };
        # Privacy Badger:
        "jid1-MnnxcxisBPnSXQ@jetpack" = {
          install_url = "https://addons.mozilla.org/firefox/downloads/latest/privacy-badger17/latest.xpi";
          installation_mode = "force_installed";
        };
        # Singlefile:
        "{531906d3-e22f-4a6c-a102-8057b88a1a63}" = {
          install_url = "https://addons.mozilla.org/firefox/downloads/latest/single-file/latest.xpi";
          installation_mode = "force_installed";
        };
        # Finnish Language Dictionary:
        "fi-FI@dictionaries.addons.mozilla.org" = {
          install_url = "https://addons.mozilla.org/firefox/downloads/latest/finnish-spellchecker-dict/latest.xpi";
          installation_mode = "force_installed";
        };
        # German Language Dictionary:
        "de-DE@dictionaries.addons.mozilla.org" = {
          install_url = "https://addons.mozilla.org/firefox/downloads/latest/dictionary-german/latest.xpi";
          installation_mode = "force_installed";
        };
      };
      # TODO: Add some add-on configuration, e.g. extra filter lists in ublock
      "3rdParty" = {
        Extensions = {
          "uBlock0@raymondhill.net" = {
            # See https://github.com/gorhill/uBlock/wiki/Deploying-uBlock-Origin:-configuration
            toOverwrite = {
              filterLists = [
                # See https://github.com/gorhill/uBlock/blob/master/assets/assets.json
                # Default lists
                "user-filters"
                "ublock-filters"
                "ublock-badware"
                "ublock-privacy"
                "ublock-abuse"
                "ublock-unbreak"
                "easylist"
                "easyprivacy"
                "urlhaus-1"
                "plowe-0"
                # Extra lists to enable
                "ublock-annoyances"
                "easylist-newsletters"
                "easylist-notifications"
              ];
            };
          };
        };
      };
      FirefoxHome = {
        Search = true;
        TopSites = false;
        SponsoredTopSites = false;
        Highlights = false;
        Pocket = false;
        SponsoredPocket = false;
        Snippets = false;
      };
      FirefoxSuggest = {
        WebSuggestions = false;
        SponsoredSuggestions = false;
        ImproveSuggest = false;
      };
      # TODO: switch to ManagedBookmarks as this will be dropped at some point https://mozilla.github.io/policy-templates/#managedbookmarks
      Bookmarks = [
        {
          Title = "NixOS Wiki";
          Placement = "toolbar";
          URL = "https://nixos.wiki/";
        }
        {
          Title = "NixOS pkgs Search";
          Placement = "toolbar";
          URL = "https://search.nixos.org/";
        }
        {
          Title = "NixOS options";
          Placement = "toolbar";
          URL = "https://search.nixos.org/options?";
        }
        {
          Title = "NixOS home-manager options";
          Placement = "toolbar";
          URL = "https://home-manager-options.extranix.com/";
        }
        {
          Title = "Oikofix";
          Placement = "toolbar";
          URL = "https://oikofix.com/";
        }
      ];
    };
  };
}
