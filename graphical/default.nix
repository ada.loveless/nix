{ config, lib, pkgs, home-manager, ... }:

{
  # Configuration and programs that should be present on graphical hosts (i.e. desktops and laptops)
  imports = [
    ../common
    ./firefox.nix
    ./thunderbird.nix
  ];

  security.polkit.enable = true;

  services.udev.extraRules = ''
      ACTION=="remove", SUBSYSTEM=="usb", ENV{ID_VENDOR_FROM_DATABASE}=="Yubico.com", RUN+="${pkgs.systemd}/bin/loginctl lock-sessions"
  '';

  systemd.user.services.polkit-gnome-authentication-agent-1 = {
    description = "polkit-gnome-authentication-agent-1";
    wantedBy = [ "graphical-session.target" ];
    wants = [ "graphical-session.target" ];
    after = [ "graphical-session.target" ];
    serviceConfig = {
      Type = "simple";
      ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
      Restart = "on-failure";
      RestartSec = 1;
      TimeoutStopSec = 10;
    };
  };

  networking.networkmanager.enable = true;

  services = {
    pipewire = {
      enable = true;
      alsa.enable = true;
      audio.enable = true;
      pulse.enable = true;
      socketActivation = true;
    };
    gvfs.enable = true;
    tumbler.enable = true;
    printing.enable = true;
    greetd = {
      enable = true;
      settings = {
        terminal = {
          vt = 1;
        };
        default_session = {
          command = "${pkgs.greetd.tuigreet}/bin/tuigreet --cmd sway";
          user = "greeter";
        };
      };
    };
  };


  fonts = {
    packages = with pkgs; [
      crimson
      b612
      google-fonts
      atkinson-hyperlegible
      fira-code-nerdfont
      noto-fonts-color-emoji
      noto-fonts-monochrome-emoji
      noto-fonts
      noto-fonts-cjk-sans
    ];
    fontconfig = {
      enable = true;
      defaultFonts = {
        serif = [ "Crimson" ];
        sansSerif = [ "Atkinson Hyperlegible" ];
        monospace = [ "Atkinson Hyperlegible Mono" ];
        emoji = [ "Noto Monochrome Emoji" ];
      };
    };
    fontDir.enable = true;
  };

  programs = {
    light.enable = true;
    sway.enable = true;
    dconf.enable = true;
    nm-applet.enable = true;
    file-roller.enable = true;
    xfconf.enable = true;
    thunar = {
      enable = true;
      plugins = with pkgs.xfce; [
        thunar-archive-plugin
        thunar-volman
      ];
    };
  };
}
