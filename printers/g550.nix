{ config, lib, pkgs, ... }:

{
  services.printing.drivers = with pkgs; [ cnijfilter2 ];
  environment.systemPackages = with pkgs; [
    cnijfilter2
  ];
}
