{ config, lib, pkgs, ... }:

{
  hardware.printers.ensurePrinters = [
    {
      name = "Annette";
      location = "Raumzeitlabor";
      model = "annette.ppd";
      deviceUri = "ipp://annette.rzl.so/ipp/";
      description = "Semi-generic colour and duplex office printer";
      ppdOptions = {
        PageSize = "A4";
      };
    }
  ];
  services.printing.drivers = [ 
    (pkgs.writeTextDir "share/cups/model/annette.ppd" (builtins.readFile ./annette.ppd))
    pkgs.magicard-cups-driver
  ];
  # according to most sources the following option *can* be useful for debugging.
  # services.printing.logLevel = "debug";
}
