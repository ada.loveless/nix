{ config, lib, pkgs, ... }:

{
  hardware.printers.ensurePrinters = [
    {
      name = "LawPrinter";
      location = "Lawyer's Office";
      description = "Some Lawyer's Printer";
      # This is a "Canon iR-ADV C3826 UFR II".
      # This (`model`) is a confusingly named field, really this should be `ppd` or something. 
      # especially because there is a MakeModel in /etc/cups/printers.conf 
      # that has the name in it and not the ppd file
      model = "CNRCUPSIRADVC3826ZK.ppd";
      # Maybe also be this one instead, both show up in lpinfo -m with the model name
      # model = "CNRCUPSIRADVC3826ZS.ppd";
      deviceUri = "socket://192.168.123.200:9100";
    }
  ];
  services.printing.drivers = with pkgs; [
    canon-cups-ufr2
  ];
}
