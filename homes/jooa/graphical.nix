{ config, pkgs, ... }:

{
  imports = [
    ./default.nix
    ./email.nix
  ];

  services = {
    nextcloud-client.enable = true;
    nextcloud-client.startInBackground = true;
  };

  home.packages = with pkgs; [
    tidal-hifi
    strawberry
  ];


  wayland.windowManager.sway.config.output."*".bg = "~/.background.png fill";
  wayland.windowManager.sway.checkConfig = false;

  home.file.".background.png" = {
    enable = true;
    source = ../graphical/backgrounds/background-bad.png;
    target = ".background.png";
  };
}
