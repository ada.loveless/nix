{ config, pkgs, ... }:

{
  home.username = "jooa";
  home.homeDirectory = "/home/jooa";
  programs.git = {
    userEmail = "j.hooli@dkfz.de";
    userName = "Jooa";
  };
}
