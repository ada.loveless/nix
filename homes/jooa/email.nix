{ config, pkgs, ... }:

{
  accounts.email.accounts = {
    dkfz = let dkfz = config.accounts.email.accounts.dkfz; in {
      primary = true;
      thunderbird = {
        enable = true;
        perIdentitySettings = id: {
          # Make thunderbird use NTLM as authentication method for SMTP.
          "mail.smtpserver.smtp_${id}.authMethod" = 6;
          # Some saner settings
          "mail.identity.id_${id}.compose_html" = false;
          "mail.identity.id_${id}.reply_on_top" = 1;
        };
      };
      realName = "Jooa Hooli";
      address = "j.hooli@dkfz.de";
      userName = "j586i";
      imap = {
        host = "imaphost.inet.dkfz-heidelberg.de";
        port = 993;
        tls = {
          enable = true;
          useStartTls = false;
        };
      };
      smtp = {
        host = "imaphost.inet.dkfz-heidelberg.de";
        port = 587;
        tls = {
          enable = true;
          useStartTls = true;
        };
      };
      signature = {
        text = ''
          ${dkfz.realName} (they/she)
          Molecular Neurobiology
          Doctoral Researcher

          German Cancer Research Center (DKFZ)
          Foundation under Public Law
          Im Neuenheimer Feld 280
          69120 Heidelberg
          Germany
          tel: +49 6221 42-3256
          ${dkfz.address}
          www.dkfz.de
          Management Board: Prof. Dr. med. Dr. h. c. Michael Baumann, Ursula Weyrich
          VAT-ID No.: DE143293537
        '';
        showSignature = "append";
      };
    };

    UniHD = {
      primary = false;
      thunderbird = {
        enable = true;
        perIdentitySettings = id: {
          "mail.identity.id_${id}.reply_on_top" = 1;
          "mail.identity.id_${id}.compose_html" = false;
        };
      };
      realName = "Jooa";
      address = "jooa.hooli@stud.uni-heidelberg.de";
      userName = "ky182";
      imap = {
        host = "imap.urz.uni-heidelberg.de";
        port = 993;
        tls = {
          enable = true;
          useStartTls = false;
        };
      };
      smtp = {
        host = "mail.urz.uni-heidelberg.de";
        port = 587;
        tls = {
          enable = true;
          useStartTls = true;
        };
      };
    };
  };

}
