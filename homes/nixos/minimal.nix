{ config, pkgs, ... }:

{
  home.username = "nixos";
  home.homeDirectory = "/home/nixos";
  home.stateVersion = "23.11"; # Please read the comment before changing.

  home.sessionVariables = {
    EDITOR = "nvim";
    VISUAL = "nvim";
  };

  home.packages = with pkgs; [
    # tools
    zellij
    zoxide
  ];

  targets.genericLinux.enable = true;

  programs = {
    xplr.enable = true;
    gpg = {
      enable = true;
      scdaemonSettings = {
        disable-ccid = true;
        reader-port = "Yubico Yubikey";
      };
    };
    git = {
      enable = false;
      extraConfig.init.defaultBranch = "main";
    };
    zellij = {
      enableZshIntegration = true;
    };
    zsh = {
      enable = true;
      autosuggestion.enable = true;
      enableCompletion = true;
      sessionVariables = {
        # EDITOR = "nvim";
      };
      oh-my-zsh = {
        enable = true;
        theme = "half-life";
        plugins = [
          "git-prompt"
          "virtualenv"
          "toolbox"
        ];
      };
      initExtraFirst = ''
        zstyle ':omz:update' mode disabled
      '';
      initExtra = ''
        eval "$(zoxide init --cmd cd zsh)"
      '';
    };
  };

}
