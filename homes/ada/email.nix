{ config, pkgs, lib, ... }:
{
  accounts.email.accounts = {
    rbox = {
      primary = true;
      # neomutt = {
      #   enable = true;
      #   extraMailboxes = [
      #     "Archive"
      #     "Drafts"
      #     "Junk"
      #     "Sent"
      #     "Trash"
      #   ];
      # };
      thunderbird = {
        enable = true;
        perIdentitySettings = id: {
          "mail.identity.id_${id}.reply_on_top" = 1;
          "mail.identity.id_${id}.compose_html" = false;
        };
      };
      realName = "Jooa";
      address = "mail@jooa.xyz";
      aliases = [
        "info@jooa.xyz"
        "jooa@jooa.xyz"
        "ada@jooa.xyz"
      ];
      userName = "eru%jooa.me";
      imap = {
        host = "mail.runbox.com";
        port = 993;
        tls = {
          enable = true;
          useStartTls = false;
        };
      };
      smtp = {
        host = "mail.runbox.com";
        port = 587;
        tls = {
          enable = true;
          useStartTls = true;
        };
      };
      signature = {
        text = ''
          Jooa (it/she)
        '';
        showSignature = "append";
      };
    };

    UniHD = {
      primary = false;
      thunderbird = {
        enable = true;
        perIdentitySettings = id: {
          "mail.identity.id_${id}.reply_on_top" = 1;
          "mail.identity.id_${id}.compose_html" = false;
        };
      };
      realName = "Jooa";
      address = "jooa.hooli@stud.uni-heidelberg.de";
      userName = "ky182";
      imap = {
        host = "imap.urz.uni-heidelberg.de";
        port = 993;
        tls = {
          enable = true;
          useStartTls = false;
        };
      };
      smtp = {
        host = "mail.urz.uni-heidelberg.de";
        port = 587;
        tls = {
          enable = true;
          useStartTls = true;
        };
      };
    };
  };

}
