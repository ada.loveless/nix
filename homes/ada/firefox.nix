{ config, pkgs, lib, ... }:

{
  programs.firefox.profiles.default = {
    bookmarks = [
      {
        name = "Travel";
        toolbar = true;
        bookmarks = [
          {
            name = "travelynx";
            tags = [ "travel" ];
            url = "https://travelynx.de/";
          }
          {
            name = "trainsearch";
            tags = [ "travel" ];
            url = "https://trainsear.ch/";
          }
        ];
      }
      {
        name = "Social";
        toolbar = true;
        bookmarks = [
          {
            name = "catcatnya";
            tags = [ "social" ];
            url = "https://catcatnya.com/";
          }
        ];
      }
      {
        name = "NixOS";
        toolbar = true;
        bookmarks = [
          {
            name = "NixOS options";
            tags = [ "nixos" "search" ];
            url = "https://search.nixos.org/options?";
          }
          {
            name = "NixOS pkgs";
            tags = [ "nixos" "search" ];
            url = "https://search.nixos.org/";
          }
          {
            name = "Home-manager options ";
            tags = [ "nixos" "search" ];
            url = "https://home-manager-options.extranix.com/";
          }
          {
            name = "NixOS Wiki";
            tags = [ "nixos" "wiki" ];
            url = "https://nixos.wiki/";
          }
        ];
      }
    ];
  };
}
