{ config, pkgs, lib, ... }:

{
  imports = [
    ./default.nix
    ./firefox.nix
    ./email.nix
    ./sway.nix
  ];
  home.packages = with pkgs; [
    mate.atril
    evince
    webcord
    audacity
    tidal-hifi
    signal-desktop
    prusa-slicer
    darktable
    geeqie
    openscad
    scribus
    element-desktop
  ];
  services = {
    nextcloud-client.enable = true;
    nextcloud-client.startInBackground = true;
  };
  programs.nheko.enable = true;
  wayland.windowManager.sway.config.output."*".bg = "~/.background.png fill";
  wayland.windowManager.sway.checkConfig = false;

  home.file = {
    ".background.png".source = ../graphical/backgrounds/background-good.png;
  };
  systemd.user.tmpfiles.rules = let home = config.home.homeDirectory; in [
    "L ${home}/Documents - - - - ${home}/Cloud/Cyberspace/Documents"
    "L ${home}/Videos - - - - ${home}/Cloud/Cyberspace/Videos"
    "L ${home}/Photos - - - - ${home}/Cloud/Cyberspace/Photos"
    "L ${home}/Data - - - - ${home}/Cloud/Cyberspace/Data"
  ];
  # Alternatively you can use something like this, but the symlinks are less understandable
  # "Data".source = config.lib.file.mkOutOfStoreSymlink "${config.home.homeDirectory}/Cloud/Cyberspace/Data";
}
