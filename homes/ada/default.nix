{ config, pkgs, ... }:

{
  home.username = "ada";
  home.homeDirectory = "/home/ada";
  home.packages = with pkgs; [
    typst
  ];
  programs.git = {
    userEmail = "code@jooa.xyz";
    userName = "Aada";
  };
}
