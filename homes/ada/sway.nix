{ config, pkgs, ... }:

{
  wayland.windowManager.sway.extraConfig = ''
    exec ${pkgs.keepassxc}/bin/keepassxc
    for_window [app_id="org.keepassxc.KeePassXC"] move container to scratchpad
    workspace 9
    exec ${pkgs.nheko}/bin/nheko
    exec ${pkgs.signal-desktop}/bin/signal-desktop
    workspace 10
    exec ${pkgs.foot}/bin/foot ${pkgs.zellij}/bin/zellij -l monitor
    workspace 1
    exec ${pkgs.firefox}/bin/firefox
  '';
  wayland.windowManager.sway.config.assigns = {
    "9" = [
      { class = "Signal"; }
      { app_id = "nheko"; }
    ];
    "10" = [
      { class = "foot-monitor"; }
    ];
  };
}
