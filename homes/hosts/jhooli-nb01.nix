{ config, pkgs, ... }:

{
  wayland.windowManager.sway.extraConfig = ''
    bindswitch --locked lid:on output eDP-1 disable
    bindswitch --locked lid:off output eDP-1 enable
  '';
  wayland.windowManager.sway.config = {
    output = {
      eDP-1 = {
        mode = "1920x1200";
        pos = "0,0";
      };
    };
  };

  services.kanshi.settings = [
    {
      profile = {
        name = "undocked";
        outputs = [
          {
            criteria = "eDP-1";
            mode = "1920x1200";
            position = "0,0";
            transform = "normal";
            status = "enable";
          }
        ];
      };
    }
    {
      profile =
        let
          laptop = "eDP-1";
          left = "Dell Inc. DELL U2415 7MT01932078U";
          right = "Dell Inc. DELL U2415 7MT0193206HU";
        in
        {
          name = "desk-ah";
          outputs = [
            {
              criteria = laptop;
              position = "1920,1500";
              transform = "normal";
              status = "disable";
            }
            {
              criteria = left;
              mode = "1920x1200";
              position = "1920,300";
              transform = "normal";
            }
            {
              criteria = right;
              transform = "270";
              mode = "1920x1200";
              position = "3840,0";
            }
          ];
          # TODO: apply this automatically to triple monitor setups
          exec = builtins.attrValues (builtins.mapAttrs (space: screen: "${pkgs.sway}/bin/swaymsg workspace \"${space}\", move workspace to '\"${screen}\"'") {
            "5" = left;
            "4" = left;
            "3" = left;
            "2" = left;
            "1" = left;
            "8" = right;
            "7" = right;
            "6" = right;
            "9" = left;
            "10" = left;
          });
        };
    }
    {
      profile =
        let
          laptop = "eDP-1";
          left = "Samsung Electric Company C27R50x H4CNB00725J";
          right = "Samsung Electric Company C27R50x H4CNB00734D";
        in
        {
          name = "desk-float1";
          outputs = [
            {
              criteria = laptop;
              position = "960,1080";
              transform = "normal";
            }
            {
              criteria = left;
              mode = "1920x1080";
              position = "0,0";
              transform = "normal";
            }
            {
              criteria = right;
              mode = "1920x1080";
              position = "1920,0";
              transform = "normal";
            }
          ];
          exec = builtins.attrValues (builtins.mapAttrs (space: screen: "${pkgs.sway}/bin/swaymsg workspace \"${space}\", move workspace to '\"${screen}\"'") {
            "5" = left;
            "4" = left;
            "3" = left;
            "2" = left;
            "1" = left;
            "8" = right;
            "7" = right;
            "6" = right;
            "9" = laptop;
            "10" = laptop;
          });
        };
    }
    {
      profile =
        let
          laptop = "eDP-1";
          left = "Dell Inc. DELL U2415 7MT018CI2F3U";
          right = "Dell Inc. DELL U2415 7MT018860A7U";
        in
        {
          name = "desk-float2";
          outputs = [
            {
              criteria = laptop;
              mode = "1920x1200";
              position = "0,0";
              transform = "normal";
            }
            {
              criteria = left;
              mode = "1920x1200";
              position = "0,0";
              transform = "normal";
            }
            {
              criteria = right;
              mode = "1920x1200";
              position = "1920,0";
              transform = "270";
            }
          ];
          exec = builtins.attrValues (builtins.mapAttrs (space: screen: "${pkgs.sway}/bin/swaymsg workspace \"${space}\", move workspace to '\"${screen}\"'") {
            "5" = left;
            "4" = left;
            "3" = left;
            "2" = left;
            "1" = left;
            "8" = right;
            "7" = right;
            "6" = right;
            "9" = left;
            "10" = left;
          });
        };
    }
    {
      profile =
        let
          laptop = "eDP-1";
          left = "Dell Inc. DELL U2415 7MT0188R2ATS";
          right = "Dell Inc. DELL U2415 7MT0193206RU";
        in
        {
          name = "desk-fr";
          outputs = [
            {
              criteria = laptop;
              position = "960,1200";
              transform = "normal";
            }
            {
              criteria = left;
              mode = "1920x1200";
              position = "0,0";
              transform = "normal";
            }
            {
              criteria = right;
              mode = "1920x1200";
              position = "1920,0";
              transform = "normal";
            }
          ];
          # TODO: apply this automatically to triple monitor setups
          exec = builtins.attrValues (builtins.mapAttrs (space: screen: "${pkgs.sway}/bin/swaymsg workspace \"${space}\", move workspace to '\"${screen}\"'") {
            "5" = left;
            "4" = left;
            "3" = left;
            "2" = left;
            "1" = left;
            "8" = right;
            "7" = right;
            "6" = right;
            "9" = laptop;
            "10" = laptop;
          });
        };
    }
    {
      profile =
        let
          laptop = "eDP-1";
          left = "Dell Inc. DELL U2415 7MT01932073U";
          right = "Dell Inc. DELL U2415 7MT0186P1KEU";
        in
        {
          name = "desk-if";
          outputs = [
            {
              criteria = laptop;
              position = "0,0";
              transform = "normal";
            }
            {
              criteria = left;
              mode = "1920x1200";
              position = "1920,0";
              transform = "normal";
            }
            {
              criteria = right;
              mode = "1920x1200";
              position = "3840,0";
              transform = "normal";
            }
          ];
          # TODO: apply this automatically to triple monitor setups
          exec = builtins.attrValues (builtins.mapAttrs (space: screen: "${pkgs.sway}/bin/swaymsg workspace \"${space}\", move workspace to '\"${screen}\"'") {
            "5" = left;
            "4" = left;
            "3" = left;
            "2" = left;
            "1" = left;
            "8" = right;
            "7" = right;
            "6" = right;
            "9" = laptop;
            "10" = laptop;
          });
        };
    }
    {
      profile =
        let
          laptop = "eDP-1";
          left = "Dell Inc. DELL U2415 7MT018BA0LAU";
          right = "Dell Inc. DELL U2415 7MT0191Q079L";
        in
        {
          name = "desk-nr";
          outputs = [
            {
              criteria = laptop;
              position = "960,1200";
              transform = "normal";
            }
            {
              criteria = left;
              mode = "1920x1200";
              position = "0,0";
              transform = "normal";
            }
            {
              criteria = right;
              mode = "1920x1200";
              position = "1920,0";
              transform = "normal";
            }
          ];
          # TODO: apply this automatically to triple monitor setups
          exec = builtins.attrValues (builtins.mapAttrs (space: screen: "${pkgs.sway}/bin/swaymsg workspace \"${space}\", move workspace to '\"${screen}\"'") {
            "5" = left;
            "4" = left;
            "3" = left;
            "2" = left;
            "1" = left;
            "8" = right;
            "7" = right;
            "6" = right;
            "9" = laptop;
            "10" = laptop;
          });
        };
    }
    {
      profile =
        let
          laptop = "eDP-1";
          other = "Samsung Electric Company SyncMaster H1AK500000";
        in
        {
          name = "flip";
          outputs = [
            {
              criteria = laptop;
              position = "0,1080";
              transform = "normal";
            }
            {
              criteria = other;
              mode = "1920x1080";
              position = "0,0";
              transform = "normal";
            }
          ];
          # TODO: apply this automatically to triple monitor setups
          exec = builtins.attrValues (builtins.mapAttrs (space: screen: "${pkgs.sway}/bin/swaymsg workspace \"${space}\", move workspace to '\"${screen}\"'") {
            "1" = laptop;
            "2" = other;
            "3" = laptop;
            "4" = laptop;
            "5" = laptop;
            "6" = laptop;
            "7" = laptop;
            "8" = laptop;
            "9" = laptop;
            "10" = laptop;
          });
        };
    }

  ];

}
