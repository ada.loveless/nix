{ config, pkgs, ... }:

{
  wayland.windowManager.sway.extraConfig = ''
    bindswitch --locked lid:on output eDP-1 disable
    bindswitch --locked lid:off output eDP-1 enable
  '';
  wayland.windowManager.sway.config.workspaceOutputAssign = [
      {
        workspace = "1";
        output = "eDP-1";
      }
  ];
  services.kanshi.settings = [
    {
      profile.name = "undocked";
      profile.outputs = [
        {
          criteria = "eDP-1";
          position = "0,0";
        }
      ];
    }
    {
      profile =
        let
          laptop = "eDP-1";
          external = "LG Electronics IPS231        401NDUN5U190";
        in
        {
          name = "maths";
          outputs = [
            {
              criteria = external;
              mode = "1920x1080";
              position = "0,0";
              transform = "normal";
            }
            {
              criteria = laptop;
              position = "0,1080";
              transform = "normal";
            }
          ];
          # TODO: apply this automatically to triple monitor setups
          exec = builtins.attrValues (builtins.mapAttrs (space: screen: "${pkgs.sway}/bin/swaymsg workspace \"${space}\", move workspace to '\"${screen}\"'") {
            "5" = external;
            "4" = external;
            "3" = external;
            "2" = external;
            "1" = external;
            "8" = external;
            "7" = external;
            "6" = external;
            "9" = laptop;
            "10" = laptop;
          });
        };
    }
  ];
}
