{ config, pkgs, ... }:

{
  wayland.windowManager.sway.config = {
    output = {
      DP-1 = {
        mode = "2560x1440";
        pos = "0 550";
      };
      DP-3 = {
        mode = "2560x1440";
        pos = "2560 0";
        transform = "270";
      };
    };
    workspaceOutputAssign = [
      {
        output = "DP-1";
        workspace = "1";
      }
      {
        output = "DP-3";
        workspace = "6";
      }
    ];
  };

  services.kanshi.settings = [
    {
      profile =
        let
          left = "Dell Inc. DELL P2720DC 921TL93";
          right = "Dell Inc. DELL P2720DC B21TL93";
        in
        {
          name = "normal";
          outputs = [
            {
              criteria = left;
              mode = "2560x1440";
              position = "0,550";
              transform = "normal";
            }
            {
              criteria = right;
              mode = "2560x1440";
              position = "2560,0";
              transform = "270";
            }
          ];
          exec = builtins.attrValues (builtins.mapAttrs (space: screen: "${pkgs.sway}/bin/swaymsg workspace \"${space}\", move workspace to '\"${screen}\"'") {
            "5" = left;
            "4" = left;
            "3" = left;
            "2" = left;
            "1" = left;
            "8" = right;
            "7" = right;
            "6" = right;
            "9" = right;
            "10" = right;
          });
        };
    }
  ];

}
