{ config, pkgs, lib, ... }:

{

  # I don't really understand why this works, but it seems to be producing
  # consistent theming
  gtk = {
    enable = true;
    font = {
      name = "Atkinson Hyperlegible";
      size = 9;
      package = pkgs.atkinson-hyperlegible;
    };
    theme = {
      name = "Breeze-Dark";
      package = pkgs.libsForQt5.breeze-gtk;
    };
    iconTheme = {
      name = "breeze-dark";
      package = pkgs.libsForQt5.breeze-icons;
    };
  };

  home.sessionVariables = {
    QT_QPA_PLATFORMTHEME = "qt5ct";
  };

  qt = {
    enable = true;
    platformTheme.name = "qt5ct";
    style.package = with pkgs; [
      kdePackages.breeze
      kdePackages.qtstyleplugin-kvantum
      adwaita-qt
    ];
  };

  xdg.configFile."qt5ct/qt5ct.conf" = {
    enable = true;
    text = lib.generators.toINI { } {
      Appearance = {
        color_scheme_path = "${pkgs.libsForQt5.qt5ct}/share/qt5ct/colors/darker.conf";
        custom_palette = true;
        icon_theme = "breeze-dark";
        standard_dialogs = "default";
        style = "Breeze";
      };
      Fonts = {
        fixed = "\"Atkinson Hyperlegible Mono,9,-1,5,50,0,0,0,0,0,SemiBold\"";
        general = "\"Atkinson Hyperlegible,9,-1,5,50,0,0,0,0,0\"";

      };
    };
  };

  xdg.configFile."qt6ct/qt6ct.conf" = {
    enable = true;
    text = lib.generators.toINI { } {
      Appearance = {
        color_scheme_path = "${pkgs.kdePackages.qt6ct}/share/qt6ct/colors/darker.conf";
        custom_palette = true;
        icon_theme = "breeze-dark";
        standard_dialogs = "default";
        style = "Breeze";
      };
      Fonts = {
        fixed = "\"Atkinson Hyperlegible Mono,9,-1,5,50,0,0,0,0,0,SemiBold\"";
        general = "\"Atkinson Hyperlegible,9,-1,5,50,0,0,0,0,0\"";
      };
    };
  };
}
