{ config, lib, pkgs, home-manager, ... }:

{
  # This manages thunar configuration
  xfconf.settings = {
    thunar = {
      default-view = "ThunarCompactView";
      shortcuts-icon-size = "THUNAR_ICON_SIZE_16";
      misc-show-delete-action = true;
      misc-directory-specific-settings = true;
      misc-date-style = "THUNAR_DATE_STYLE_SIMPLE";
    };
  };
  xdg.configFile."Thunar/uca.xml".text = ''
    <?xml version="1.0" encoding="UTF-8"?>
    <actions>
    <action>
    	<icon>utilities-terminal</icon>
    	<name>Open Terminal Here</name>
    	<submenu></submenu>
    	<unique-id>1595785384840694-1</unique-id>
    	<command>${pkgs.foot}/bin/foot -D %f</command>
    	<description>Start terminal in this folder</description>
    	<range></range>
    	<patterns>*</patterns>
    	<startup-notify/>
    	<directories/>
    </action>
    </actions>
  '';
}
