local wezterm = require 'wezterm';
wezterm.on('toggle-ligature', function(window, _)
  local overrides = window:get_config_overrides() or {}
  if not overrides.harfbuzz_features then
    -- If we haven't overridden it yet, then override with ligatures disabled
    overrides.harfbuzz_features = { 'calt=0', 'clig=0', 'liga=0' }
  else
    -- else we did already, and we should disable out override now
    overrides.harfbuzz_features = nil
  end
  window:set_config_overrides(overrides)
end)
return {
    disable_default_key_bindings = true,
    color_scheme = "thistheme",
    keys = {
        {
            key = 'E',
            mods = 'CTRL',
            action = wezterm.action.EmitEvent 'toggle-ligature' -- |> => <= <| 
        },
        { key = "-", mods = "CTRL", action = wezterm.action.DecreaseFontSize },
        { key = "=", mods = "CTRL", action = wezterm.action.IncreaseFontSize },
        { key = ")", mods = "CTRL", action = wezterm.action.ResetFontSize },
        { key = "V", mods = "CTRL", action = wezterm.action.PasteFrom("Clipboard") },
        { key = "C", mods = "CTRL", action = wezterm.action.CopyTo("Clipboard") },
    },
    window_background_opacity = 0.95,
    font = wezterm.font 'Atkinson Hyperlegible Mono',
    font_size = 7.5,
    enable_tab_bar = false,
    adjust_window_size_when_changing_font_size = false,
    automatically_reload_config = true,
    bold_brightens_ansi_colors = false,
    audible_bell = "Disabled",
    mux_enable_ssh_agent = false,
    window_padding = {
        left = 2,
        right = 2,
        bottom = 2,
        top = 2,
    },
    enable_scroll_bar = false,
    enable_wayland = true,
    window_close_confirmation = "NeverPrompt",
    front_end="OpenGL",
}
