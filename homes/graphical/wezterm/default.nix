{ config, pkgs, ... }:

{
  programs.wezterm = {
    enable = true;
    enableZshIntegration = true;
    colorSchemes = {
      thistheme = {
        ansi = [
          "#101012" # 0
          "#de5d68" # 1
          "#8ad454" # 2
          "#ead050" # 3
          "#47a5e5" # 4
          "#bb70d2" # 6
          "#51a8b3" # 7
          "#848484" # 8
          # 7  #848484
        ];
        brights = [
          "#a7aab0" # 9
          "#a34141" # 10
          "#638848" # 11
          "#cdc322" # 12
          "#1e77c1" # 13
          "#79428a" # 14
          "#328f96" # 15
          "#5c5c5c" # 16
        ];
        foreground = "#ffffff";
        background = "#111111";
        cursor_bg = "#ffffff";
        cursor_fg = "#000000";
        selection_fg = "black";
        selection_bg = "#fffacd";
      };
    };
    extraConfig = builtins.readFile ./wezterm.lua;
  };
}
