{ config, pkgs, lib, ... }:

{
  programs.firefox = {
    enable = true;
    profiles.default.settings = {
      "font.name.serif.x-western" = "Crimson";
      "font.name.sans-serif.x-western" = "Atkinson Hyperlegible";
      "font.name.monospace.x-western" = "Atkinson Hyperlegible Mono";
      "font.size.variable.x-western" = 14;
      # "browser.display.use_document_fonts" = 0;
    };
  };
}
