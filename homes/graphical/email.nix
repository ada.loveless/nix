{ config, pkgs, ... }:

{
  programs.thunderbird = {
    enable = true;
    profiles = {
      default = {
        isDefault = true;
      };
    };
    settings = {
      "browser.tabs.inTitlebar" = 0;
      "mail.uidensity" = 0;
      # "mail.folder.views.version" = 0;
      "mail.tabs.drawInTitlebar" = false;
      "mail.biff.play_sound" = false;
      "mail.biff.use_system_alert" = true;
      "font.name.serif.x-western" = "Crimson";
      "font.name.sans-serif.x-western" = "Atkinson Hyperlegible";
      "font.name.monospace.x-western" = "Atkinson Hyperlegible Mono";
      "font.size.variable.x-western" = 14;
      # "mailnews.start_page_override.mstone" = "115.8.1";
      # "mailnews.tags.version" = 2;
    };
  };
  # programs.neomutt = {
  #   enable = true;
  #   vimKeys = true;
  #   sidebar = {
  #     enable = true;
  #     width = 30;
  #   };
  #   settings = {
  #     mark_old = "no";
  #     text_flowed = "yes";
  #     reverse_name = "yes";
  #   };
  # };
}
