{ config, pkgs, ... }:

{
  home.packages = [ pkgs.kanshi ];
  services.kanshi = {
    enable = true;
  };
}
