{ config, pkgs, ... }:

{
  imports = [
    ./common.nix
    ./more.nix
  ];
  programs.waybar.settings.mainBar = {
    modules-left = [
      "sway/workspaces"
      "sway/scratchpad"
      "sway/mode"
    ];
    modules-center = [
      "clock"
    ];
    modules-right = [
      "idle_inhibitor"
      "pulseaudio"
      "cpu"
      "memory"
      "temperature"
      "tray"
    ];
  };
}
