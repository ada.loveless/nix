{ config, pkgs, ... }:
let 
  rofiYkman = pkgs.writeShellScript "rofi-ykman" ''
    set -euo pipefail

    account="$(${pkgs.yubikey-manager}/bin/ykman oath accounts list | rofi -dmenu)"
    token="$(${pkgs.yubikey-manager}/bin/ykman oath accounts code -s "$account" | tr -d '\n' )"
    printf "$token" | ${pkgs.wl-clipboard}/bin/wl-copy
    ${pkgs.libnotify}/bin/notify-send --urgency=normal --expire-time=5000 "Yubikey OATH" "Copied Token $token ($account) to clipboard"
  '';
  trainTool = pkgs.writeShellScript "train-search" ./train-search.sh;
  volume = pkgs.writeShellScript "volume" ''
    set -euo pipefail

    case "$1" in
      up)
        ${pkgs.pulseaudio}/bin/pactl set-sink-volume @DEFAULT_SINK@ +5%
      ;;
      down)
        ${pkgs.pulseaudio}/bin/pactl set-sink-volume @DEFAULT_SINK@ -5%
      ;;
      mute)
        ${pkgs.pulseaudio}/bin/pactl set-sink-mute @DEFAULT_SINK@ toggle
      ;;
      *)
        exit 1
      ;;
    esac
    mute="$(${pkgs.pulseaudio}/bin/pactl get-sink-mute @DEFAULT_SINK@ | grep -Po '(yes|no)')"
    volume="$(${pkgs.pulseaudio}/bin/pactl get-sink-volume @DEFAULT_SINK@ | grep -Po '\d+%' | tr -d '%' | head -1)"
    case "$mute" in 
      no)
        notify-send "Volume" "$volume%" -h int:value:$volume -h string:synchronous:volume
      ;;
      yes)
        notify-send "Volume" "muted" -h int:value:0 -h string:synchronous:volume
      ;;
    esac
  '';
  brightness = pkgs.writeShellScript "brightness" ''
    set -euo pipefail

    case "$1" in
      up)
        ${pkgs.light}/bin/light -A 5
      ;;
      down)
        ${pkgs.light}/bin/light -U 5
      ;;
      *)
        exit 1
      ;;
    esac
    brightness="$(${pkgs.light}/bin/light | cut -f1 -d'.')"
    notify-send "Backlight" -h int:value:$brightness -h string:synchronous:volume
  '';
in
{
  wayland.windowManager.sway.config = {
    modifier = "Mod4";
    left = "h";
    down = "t";
    up = "n";
    right = "s";
    modes = {
      resize = {
        Left = "resize shrink width 10px";
        Down = "resize grow height 10px";
        Up = "resize shrink height 10px";
        Right = "resize grow width 10px";
        # Ditto, with arrow keys
        h = "resize shrink width 10px";
        t = "resize grow height 10px";
        n = "resize shrink height 10px";
        s = "resize grow width 10px";
        # Return to default mode
        Return = "mode \"default\"";
        Escape = "mode \"default\"";
      };
    };
    keybindings = let conf = config.wayland.windowManager.sway.config;
    in {
      # Basics
      "${conf.modifier}+Return" = "exec ${conf.terminal}";
      "${conf.modifier}+Shift+Return" = "exec ${conf.terminal} -a floating-foot";
      "${conf.modifier}+Shift+apostrophe" = "kill";
      "${conf.modifier}+e" = "exec ${conf.menu}";
      "${conf.modifier}+Shift+j" = "reload";
      # Movement
      "${conf.modifier}+${conf.left}" = "focus left";
      "${conf.modifier}+${conf.down}" = "focus down";
      "${conf.modifier}+${conf.up}" = "focus up";
      "${conf.modifier}+${conf.right}" = "focus right";
      "${conf.modifier}+Left" = "focus left";
      "${conf.modifier}+Down" = "focus down";
      "${conf.modifier}+Up" = "focus up";
      "${conf.modifier}+Right" = "focus right";
      # Move windows
      "${conf.modifier}+Shift+${conf.left}" = "move left";
      "${conf.modifier}+Shift+${conf.down}" = "move down";
      "${conf.modifier}+Shift+${conf.up}" = "move up";
      "${conf.modifier}+Shift+${conf.right}" = "move right";
      "${conf.modifier}+Shift+Left" = "move left";
      "${conf.modifier}+Shift+Down" = "move down";
      "${conf.modifier}+Shift+Up" = "move up";
      "${conf.modifier}+Shift+Right" = "move right";
      # Workspace movement
      "${conf.modifier}+1" = "workspace number 1";
      "${conf.modifier}+2" = "workspace number 2";
      "${conf.modifier}+3" = "workspace number 3";
      "${conf.modifier}+4" = "workspace number 4";
      "${conf.modifier}+5" = "workspace number 5";
      "${conf.modifier}+6" = "workspace number 6";
      "${conf.modifier}+7" = "workspace number 7";
      "${conf.modifier}+8" = "workspace number 8";
      "${conf.modifier}+9" = "workspace number 9";
      "${conf.modifier}+0" = "workspace number 10";
      "${conf.modifier}+Shift+1" = "move container to workspace number 1";
      "${conf.modifier}+Shift+2" = "move container to workspace number 2";
      "${conf.modifier}+Shift+3" = "move container to workspace number 3";
      "${conf.modifier}+Shift+4" = "move container to workspace number 4";
      "${conf.modifier}+Shift+5" = "move container to workspace number 5";
      "${conf.modifier}+Shift+6" = "move container to workspace number 6";
      "${conf.modifier}+Shift+7" = "move container to workspace number 7";
      "${conf.modifier}+Shift+8" = "move container to workspace number 8";
      "${conf.modifier}+Shift+9" = "move container to workspace number 9";
      "${conf.modifier}+Shift+0" = "move container to workspace number 10";
      # Layouts
      "${conf.modifier}+d" = "splith";
      "${conf.modifier}+k" = "splitv";
      "${conf.modifier}+o" = "layout stacking";
      "${conf.modifier}+comma" = "layout tabbed";
      "${conf.modifier}+period" = "layout toggle split";
      "${conf.modifier}+u" = "fullscreen";
      "${conf.modifier}+Shift+space" = "floating toggle";
      "${conf.modifier}+space" = "focus mode_toggle";
      "${conf.modifier}+a" = "focus parent";
      "${conf.modifier}+Shift+a" = "focus child";
      # Scratchpad
      "${conf.modifier}+minus" = "scratchpad show";
      "${conf.modifier}+Shift+minus" = "move scratchpad";
      # Media keys
      # Volume
      "XF86AudioRaiseVolume" = "exec ${volume} up";
      "XF86AudioLowerVolume" = "exec ${volume} down";
      "XF86AudioMute" = "exec ${volume} mute";
      # Brightness
      "XF86MonBrightnessDown" = "exec ${brightness} down";
      "XF86MonBrightnessUp" = "exec ${brightness} up";
      "XF86Display" = "exec ${pkgs.kanshi}/bin/kanshictl reload";
      # Player Control
      "XF86AudioMicMute" = "exec ${pkgs.pulseaudio}/bin/pactl set-source-mute @DEFAULT_SOURCE@ toggle";
      "XF86AudioPlay" = "exec ${pkgs.playerctl}/bin/playerctl play-pause";
      "XF86AudioNext" = "exec ${pkgs.playerctl}/bin/playerctl next";
      "XF86AudioPrev" = "exec ${pkgs.playerctl}/bin/playerctl previous";
      "XF86AudioStop" = "exec ${pkgs.playerctl}/bin/playerctl stop";
      # Modes
      "${conf.modifier}+p" = "mode \"resize\"";
      # Lock screen
      "${conf.modifier}+Shift+m" = "exec ${pkgs.swaylock-effects}/bin/swaylock";
      # PrintSCreen
      "Print" = "exec ${pkgs.grim}/bin/grim -g \"$(${pkgs.slurp}/bin/slurp -d)\" - | wl-copy -t image/png";
      # Other
      "${conf.modifier}+Shift+r" = "exec ${pkgs.rofimoji}/bin/rofimoji";
      "${conf.modifier}+Shift+c" = "exec ${rofiYkman}";
      "${conf.modifier}+Shift+g" = "exec ${trainTool}";
      "${conf.modifier}+Shift+period" = "exec rofi -show power-menu -modi power-menu:rofi-power-menu  -theme-str 'window {width: 8em;} listview {lines: 6;}'";
    };
  };
}
