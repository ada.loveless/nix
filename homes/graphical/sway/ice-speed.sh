#!/usr/bin/env bash
set -euo pipefail
# The above two are also set by nixpkgs.writeShellApplication, but I include them anyway.

state="$(nmcli -f general.state connection show WIFIonICE)"

if [[ "$state" =~ "activ" ]]
then
  trainInfo="$(curl https://iceportal.de/api1/rs/status)"
  speed="$(echo "$trainInfo" | jq .speed | cut -d'.' -f1)"
  case "$speed" in
    [0-9]) speedIcon="\udb83\udf86  ";;
    [0-9][0-9]) speedIcon="\udb83\udf86  ";;
    1[0-9][0-9]) speedIcon="\udb83\udf85  ";;
    2[0-9][0-9]) speedIcon="\udb81\udcc5  ";;
    3[0-9][0-9]) speedIcon="\udb81\udcc5  ";;
    *) speedIcon="";;
  esac
  case "$(echo "$trainInfo" | jq .connectivity.currentState | tr -d '"')" in
    HIGH) conn="\udb82\udcbe  ";;
    MIDDLE) conn="\udb82\udcbd  ";;
    WEAK) conn="\udb82\udcbc  ";;
    OFFLINE) conn="\udb81\udf83  ";;
    NO_INFO) conn="?  ";;
    *) conn="?  ";;
  esac
  tooltip="$(echo "$trainInfo" | jq . | jq -Rsa .)"
  echo "{\"text\": \"$conn$speedIcon$speed km/h\", \"tooltip\": $tooltip}"
else
  echo "{\"text\": \" No train :( \", \"tooltip\": \"This is not WIFIonICE\"}"
fi
