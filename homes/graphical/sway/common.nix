{ config, pkgs, ... }:

{
  imports = [
    ./keyboard.nix
  ];

  home.packages = with pkgs; [
    grim # screenshots
    slurp # screenshot region selection
    #playerctl
    wl-clipboard # for wl-paste and wl-copy
    wev # for event inspection in wayland
    wl-mirror
    # pulseaudio # for pactl
    libnotify # for notify-send
    ## In NixOS config
    # light # for backlight control
    ## managed by a config
    # rofi 
    # rofi-wayland
    rofi-power-menu
  ];

  programs = {
    swaylock = {
      enable = true;
      settings = {
        indicator-radius = 50;
        indicator-thickness = 10;
        inside-color = "00000099";
        ring-color = "00000099";
        separator-color = "00000099";
        color = "000000";
        clock = true;
        # indicator = true;
        # indicator-image = "/home/ada/Downloads/kirby-pat.git";
      };
    };
  };

  wayland.windowManager.sway = {
    enable = true;
    config = {
      defaultWorkspace = "workspace number 1";
      terminal = "foot";
      menu = "rofi -terminal 'foot' -show combi -combi-modes drun#run -modes combi";
      input = {
        "*" = {
          xkb_variant = "dvorak";
          xkb_layout = "gb";
          xkb_options = "caps:ctrl_modifier,compose:ralt";
        };
      };
      bars = [
        {
          position = "top";
          command = "waybar";
        }
      ];
      fonts = {
        names = [ "Atkinson Hyperlegible" "Atkinson Hyperlegible Mono" ];
        style = "Regular";
        size = 8.0;
      };
      floating = {
        criteria = [
          { app_id = "org.pulseaudio.pavucontrol"; }
          { app_id = "floating-foot"; }
          # Grrr how to get thunderbird calendar things to do this
          { app_id = "(?i)Thunderbird"; title = ".*Reminder"; }
          { window_role = "pop-up"; }
          { window_role = "Pop-up"; }
          { window_role = "bubble"; }
          { window_role = "Bubble"; }
          { window_role = "dialog"; }
          { window_role = "Dialog"; }
          { window_role = "About"; }
          { window_role = "task_dialog"; }
          { window_type = "menu"; }
          { class = "dialog"; }
          { class = "Dialog"; }
        ];
      };
      startup = [
        { command = "exec systemctl --user set-environment XDG_CURRENT_DESKTOP=sway"; }
        { command = "exec systemctl --user import-environment DISPLAY SWAYSOCK WAYLAND_DISPLAY XDG_CURRENT_DESKTOP"; }
        { command = "exec hash dbus-update-activation-environment 2>/dev/null && dbus-update-activation-environment --systemd DISPLAY SWAYSOCK XDG_CURRENT_DESKTOP=sway WAYLAND_DISPLAY"; }
      ];
      # Style
      colors = {
        focused = {
          border = "#12709f";
          background = "#12709f";
          text = "#ffffff";
          indicator = "#1773ff";
          childBorder = "#12709f";
        };
        focusedInactive = {
          border = "#333333";
          background = "#333333";
          text = "#f9f9f9";
          indicator = "#484e50";
          childBorder = "#333333";
        };
        unfocused = {
          border = "#444444";
          background = "#444444";
          text = "#eeeeee";
          indicator = "#eeeeee";
          childBorder = "#444444";
        };
        urgent = {
          border = "#ff0000";
          background = "#ff0000";
          text = "#ffffff";
          indicator = "#900000";
          childBorder = "#ff0000";
        };
        placeholder = {
          border = "#000000";
          background = "#0c0c0c";
          text = "#ffffff";
          indicator = "#000000";
          childBorder = "#000000";
        };
      };
    };
    extraConfig = ''
      titlebar_border_thickness 1
      titlebar_padding 5 1
    '';
  };

  programs.waybar = {
    enable = true;
    settings = {
      mainBar = {
        spacing = 0;
        "sway/language" = {
          "tooltip" = false;
          "format" = "{short} {variant}";
          "on-click" = "swaymsg input type:keyboard xkb_switch_layout next";
        };
        "sway/mode" = {
          "tooltip" = false;
          "format" = "<span color=\"#ff0000\">{}</span>";
        };
        "sway/scratchpad" = {
          "format" = "{icon}";
          "show-empty" = false;
          "format-icons" = [ "" "" ];
          "tooltip" = true;
          "tooltip-format" = "{app}: {title}";
        };
        "idle_inhibitor" = {
          "format" = "{icon}";
          "format-icons" = {
            "activated" = "";
            "deactivated" = "";
          };
        };
        "tray" = {
          "icon-size" = 14;
          "spacing" = 1;
        };
        "clock" = {
          "interval" = 1;
          "format" = "🐈‍⬛ {:%Y-%m-%d %H:%M:%S} 🐈‍⬛";
          "tooltip-format" = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
          # 😺😸😹😻😼😽🙀😿😾
        };
        "cpu" = {
          "format" = "{usage}% ";
          "tooltip" = false;
        };
        "memory" = {
          "format" = "{}% ";
        };
        "temperature" = {
          "critical-threshold" = 80;
          "format" = "{temperatureC}°C {icon}";
          "format-icons" = [ "" "" "" ];
        };
        "backlight" = {
          "format" = "{percent}% {icon}";
          "format-icons" = [ "" "" "" "" "" "" "" "" "" ];
        };
        "battery" = {
          "states" = {
            "warning" = 30;
            "critical" = 15;
          };
          "format" = "{capacity}% {icon}";
          "format-charging" = "{capacity}% 󰂄";
          "format-plugged" = "{capacity}% ";
          "format-alt" = "{time} {icon}";
          "format-icons" = [ "" "" "" "" "" ];
        };
        "pulseaudio" = {
          "scroll-step" = 1;
          "format" = "{volume}% {icon} {format_source}";
          "format-bluetooth" = "{volume}% {icon} {format_source}";
          "format-bluetooth-muted" = "󰝟 {icon} {format_source}";
          "format-muted" = "󰝟 {format_source}";
          "format-source" = "";
          "format-source-muted" = "";
          "format-icons" = {
            "headphone" = "";
            "hands-free" = "󰋎";
            "headset" = "󰋎";
            "phone" = "";
            "portable" = "";
            "car" = "";
            "default" = [ "" "" "" ];
          };
          "on-click" = "pavucontrol";
        };
      };
    };
    style = builtins.readFile ./waybar.css;
  };
}
