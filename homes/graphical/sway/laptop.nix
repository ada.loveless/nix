{ config, pkgs, ... }:

let
  ice-speed = pkgs.writeShellApplication {
    name = "ice-speed";
    runtimeInputs = with pkgs; [ networkmanager jq curl coreutils ];
    text = builtins.readFile ./ice-speed.sh;
  };
in
{
  imports = [
    ./common.nix
    ./more.nix
  ];
  programs.waybar.settings.mainBar = {
    modules-left = [
      "sway/workspaces"
      "sway/scratchpad"
      "sway/mode"
      "custom/travelynx"
    ];
    modules-center = [
      "clock"
    ];
    modules-right = [
      "custom/speed"
      "idle_inhibitor"
      "pulseaudio"
      "cpu"
      "memory"
      "temperature"
      "backlight"
      "battery"
      "tray"
    ];
    "custom/travelynx" = {
      exec = "${pkgs.travelynx}/bin/travelynx status -c -f ~/.travelynx";
      return-type = "json";
      tooltip = true;
    };
    "custom/speed" = {
      exec = "${ice-speed}/bin/ice-speed";
      interval = 10;
      return-type = "json";
      tooltip = true;
    };
  };
}
