{ config, pkgs, ... }:

{
  imports = [
    ./common.nix
  ];
  programs.waybar.settings.mainBar = {
    modules-left = [
      "sway/workspaces"
      "sway/scratchpad"
      "sway/mode"
    ];
    modules-center = [
      "clock"
    ];
    modules-right = [
      "idle_inhibitor"
      "cpu"
      "memory"
      "backlight"
      "battery"
      "tray"
    ];
  };
}
