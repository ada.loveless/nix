#!/usr/bin/env bash

set -xeuo pipefail

IFS=$'\n'

# options=(
#   bahn.expert
#   bahnhof.de
# )
#
# choice="$(echo "${options[*]}" | rofi -dmenu)"
choice=bahnhof.de

case "$choice" in
  "") exit 0;;
  bahnhof.de)
    NONCE=91473b6e8dc173127f972248841c01067e64df55

    query="$(rofi -dmenu -p "Station Search" -l 1)"
    response=$(curl "https://www.bahnhof.de/suche" \
        -sS \
        -X POST \
        -H "Next-Action: $NONCE" \
        -H "Content-Type: text/plain;charset=UTF-8" \
        --data-raw "[\"$query\"]")
    response_json="$(echo "$response" | tail -1 | cut -d':' -f2-)"

    choice="$(echo "$response_json" | jq -er '.[].name' | rofi -dmenu -format i -i -p "Stations")"
    slug="$(echo "$response_json" | jq -er ".[$choice].slug")"
    uri="https://www.bahnhof.de/$slug"
    ;;
  *)
    rofi -e "Not supported"
    ;;
esac
xdg-open "$uri"
