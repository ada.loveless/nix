{ config, pkgs, ... }:

let
  lock-cmd = "${pkgs.swaylock-effects}/bin/swaylock";
  unlock-cmd = "pkill -xu \"$USER\" -SIGUSR swaylock";
in
{
  services = {
    gnome-keyring = {
      enable = true;
      components = [ "secrets" ];
    };
    swayidle =
      {
        enable = true;
        events = [
          { event = "lock"; command = lock-cmd; }
          { event = "unlock"; command = unlock-cmd; }
          { event = "before-sleep"; command = lock-cmd; }
        ];
        timeouts = [
          { 
            timeout = 300; 
            command = lock-cmd; 
          }
          {
            timeout = 360;
            command = "${pkgs.sway}/bin/swaymsg \"output * power off\"";
            resumeCommand = "${pkgs.sway}/bin/swaymsg \"output * power on\"";
          }
        ];
      };
  };
}
