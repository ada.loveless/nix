# User-home config that is adds common graphical things
{ config, pkgs, lib, ... }:

{
  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.

  imports = [
    ../common
    ./wezterm
    ./thunar
    ./foot.nix
    ./email.nix
    ./firefox.nix
    ./kanshi.nix
    ./notifications.nix
    ./style.nix
  ];

  home.packages = with pkgs; [
    todoman
    # gui tools
    libreoffice
    zathura
    pdfpc
    mate.atril
    seahorse
    pavucontrol
    keepassxc
    inkscape-with-extensions
    gimp
    gimpPlugins.gmic
    imv
    mpv
    yubikey-manager-qt
    wdisplays
    libsForQt5.qt5ct
    tidal-hifi
    networkmanagerapplet # necessary for applet showing correct icons
  ];

  systemd.user.tmpfiles.rules = let home = config.home.homeDirectory; in [
    # Autoclean ~/Downloads
    "d ${home}/Downloads - - - 10d -"
  ];
  # Alternatively, the manual version of this
  # services.autoclean-downloads = {
  #   enable = true;
  #   age = 14;
  # };

  xdg.mimeApps.defaultApplications = {
    "application/pdf" = [ "org.pwmt.zathura.desktop" ];
    "image/svg+xml" = [ "org.inkscape.Inkscape.desktop firefox.desktop" ];
    "video/*" = [ "mpv.desktop" ];
    "image/*" = [ "imv-dir.desktop" ];
    "image/jpeg" = [ "imv-dir.desktop" ];
    "image/png" = [ "imv-dir.desktop" ];
    "text/html" = [ "firefox.desktop" ];
  };

  services = {
    network-manager-applet.enable = true;
    blueman-applet.enable = true;
  };

  xdg.configFile."keepassxc/keepassxc.ini".text = lib.generators.toINI { } {
    General = {
      ConfigVersion = 2;
    };
    GUI = {
      CompactMode = true;
    };
    Security = {
      LockDatabaseIdle = true;
      LockDatabaseIdleSeconds = 300;
    };
  };

  fonts.fontconfig.enable = true;

  programs = {
    rofi = {
      enable = true;
      font = "Atkinson Hyperlegible 8";
      terminal = "foot";
      theme = "Arc-Dark";
      package = pkgs.rofi-wayland;
    };
  };
}
