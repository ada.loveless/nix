{ config, pkgs, lib, ... }:

{
  programs.foot = {
    enable = true;
    settings = {
      main = {
        # term = "xterm-256color";
        font = "Atkinson Hyperlegible Mono:size=6.5:weight=SemiBold";
        font-bold = "Atkinson Hyperlegible Mono:size=6.5:weight=Bold";
        font-italic = "Atkinson Hyperlegible Mono:size=6.5:slant=Italic:weight=SemiBold";
        font-bold-italic = "Atkinson Hyperlegible Mono:size=6.5:slant=Italic:weight=Bold";
        font-size-adjustment=1;
      };
      colors = {
        background = "111111";
        foreground = "ffffff";
        regular0 = "101012"; # 0
        regular1 = "de5d68"; # 1
        regular2 = "8ad454"; # 2
        regular3 = "ead050"; # 3
        regular4 = "47a5e5"; # 4
        regular5 = "bb70d2"; # 6
        regular6 = "51a8b3"; # 7
        regular7 = "848484"; # 8
        bright0 = "a7aab0"; # 9
        bright1 = "a34141"; # 10
        bright2 = "638848"; # 11
        bright3 = "cdc322"; # 12
        bright4 = "1e77c1"; # 13
        bright5 = "79428a"; # 14
        bright6 = "328f96"; # 15
        bright7 = "5c5c5c"; # 16
        alpha = 0.95;
      };
      cursor = {
        style = "block";
        blink = true;
      };
      mouse = {
        hide-when-typing = "yes";
      };
    };
  };
}
