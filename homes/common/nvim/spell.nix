{ config, pkgs, ... }:

let
  nvim-spell-de-utf8-dictionary = builtins.fetchurl {
    url = "http://ftp.vim.org/vim/runtime/spell/de.utf-8.spl";
    sha256 = "73c7107ea339856cdbe921deb92a45939c4de6eb9c07261da1b9dd19f683a3d1";
  };
  nvim-spell-de-utf8-suggestions = builtins.fetchurl {
    url = "http://ftp.vim.org/vim/runtime/spell/de.utf-8.sug";
    sha256 = "13d0ecf92863d89ef60cd4a8a5eb2a5a13a0e8f9ba8d1c6abe47aba85714a948";
  };
in
{
  xdg.configFile = {
    "nvim/spell/de-utf-8.spl".source = nvim-spell-de-utf8-dictionary;
    "nvim/spell/de-utf-8.sug".source = nvim-spell-de-utf8-suggestions;
  };
}
