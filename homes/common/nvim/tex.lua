vim.opt.conceallevel = 2
vim.opt.sw = 2
-- folds
vim.opt.spell = true
vim.g.vimtex_fold_enabled = 1
vim.g.vimtex_fold_manual = 1
vim.g.vimtex_fold_types = {
  preamble = { enabled = 0 },
  sections = {
    parse_levels = 0,
    sections = {
      "%(add)?part",
      "%(chapter|addchap)",
      "%(section|addsec)",
      "subsection",
      "subsubsection"
    },
  },
  cmd_single_opt = {
    enabled = 1
  }
}
-- format
vim.g.vimtex_format_enabled = 1
vim.g.vimtex_imaps_enabled = 0
-- Indent
vim.g.vimtex_indent_enabled = 1
vim.g.vimtex_indent_bib_enabled = 1
vim.g.vimtex_indent_tikz_commands = 1
vim.g.vimtex_indent_on_ampersands = 1

-- Compiler
vim.g.vimtex_compiler_enabled = 1

local function set_compiler_method(mainfile)
  if vim.fn.filereadable("Makefile") then
    -- Makefile existing probably means we need make
    return "generic"
  end
  if vim.fn.filereadable(mainfile) == 1 then
    -- Try to infer from the main file
    local head = vim.fn.readfile(mainfile, '', 5)
    for _, line in ipairs(head) do
      if string.match(line, "%%%s*arara") then
        return "arara"
      end
    end
  end
  if vim.fn.executable("tectonic") then
    -- Default fallback is tectonic. This usually works.
    return "tectonic"
  end
  if vim.fn.executable("latexmk") then
    -- Default fallback is tectonic. This usually works.
    return "latexmk"
  end
  return "generic"
end
vim.g.vimtex_compiler_method = set_compiler_method

vim.g.vimtex_compiler_tectonic = {
  out_dir = "build",
}
vim.g.vimtex_compiler_arara = {
  options = { "--log" },
}
vim.g.vimtex_compiler_generic = {
  command = "make"
}

-- motion
vim.g.vimtex_motion_enabled = 1

-- We want LSP to do this
vim.g.vimtex_quickfix_enabled = 0

-- Syntax
vim.g.vimtex_syntax_enabled = 1
-- Conceal
vim.g.vimtex_syntax_conceal = {
  accents = 1,
  ligatures = 1,
  cites = 1,
  fancy = 0,
  spacing = 0,
  greek = 0,
  math_bounds = 0,
  math_delimiters = 0,
  math_fracs = 0,
  math_super_sub = 0,
  math_symbols = 0,
  sections = 0,
  styles = 0,
}
vim.g.vimtex_syntax_conceal_disable = 0
vim.g.vimtex_syntax_conceal_cites = {
  type = "brackets",
  verbose = "v:true",
}
vim.g.vimtex_syntax_custom_cmds = {
  { name = "gene",     conceal = true,  argstyle = "ital", argspell = false },
  { name = "person",   conceal = true,  argstyle = "ital", argspell = false },
  { name = "software", conceal = true,  argstyle = "bold", argspell = false },
  { name = "subfile",  conceal = false, argspell = false },
}
vim.g.vimtex_syntax_custom_cmds_with_concealed_delims = {
  { name = "index", nargs = 1, cchar_open = ">", cchar_close = "<", argspell = false },
}
vim.g.vimtex_syntax_nospell_comments = 1

-- toc
vim.g.vimtex_toc_enabled = 1
vim.g.vimtex_toc_config = {
  tocdepth = 5,
  layer_status = {
    content = 1,
    label = 0,
    todo = 1,
    include = 0,
  },
  split_width = 40,
  todo_sorted = 0,
  fold_enable = 1,
}
-- viewer Perhaps look at this feature in the future
vim.g.vimtex_view_enabled = 1
vim.g.vimtex_view_method = "zathura"
