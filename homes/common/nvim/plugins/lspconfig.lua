local lspconfig = require("lspconfig")
lspconfig.nil_ls.setup {
  settings = {
    ['nil'] = {
      formatting = {
        command = { "nixpkgs-fmt" }
      }
    }
  }
}
lspconfig.texlab.setup {}
lspconfig.tinymist.setup {
}
lspconfig.rust_analyzer.setup {}
lspconfig.lua_ls.setup {
  settings = {
    Lua = {
      runtime = { version = "LuaJIT" },
      diagnostics = { globals = { "vim" } },
      workspace = { library = { vim.env.VIMRUNTIME } }
    }
  }
}
lspconfig.bashls.setup {}
lspconfig.yamlls.setup {
  schemas = {
    -- You can put schemas here
    -- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#yamlls
    ["https://gitlab.com/gitlab-org/gitlab/-/raw/master/app/assets/javascripts/editor/schema/ci.json"] = ".gitlab-ci.yml",
  },
}
lspconfig.openscad_lsp.setup {}
lspconfig.r_language_server.setup {
  cmd = { "r-lsp" },
}
-- If you find this packaged in nix somewhere at some point, maybe try this
-- lspconfig.autotools_ls.setup {
--   cmd = { "autotools-language-server" }
-- }
lspconfig.pylsp.setup {
  cmd = { "pylsp" }
}
lspconfig.julials.setup{}
