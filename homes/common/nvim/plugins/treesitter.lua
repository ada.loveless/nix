require("nvim-treesitter.configs").setup {
    -- ensure_installed = {
    --   "c", "lua", "vim", "vimdoc", "query",
    --   "latex", "markdown", "markdown_inline", "make",
    --   "julia", "python", "rust", "dockerfile", "xml"
    -- },
    -- auto_install = true,
    highlight = {
        disable = { "latex", "markdown" },
        enable = true,
    },
    incremental_selection = {
        enable = true,
        keymaps = {
            init_selection = "<C-space>",
            node_incremental = "<C-space>",
            scope_incremental = false,
            node_decremental = "<bs>",
        },
    },
}
