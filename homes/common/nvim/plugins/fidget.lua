require("fidget").setup {
  progress = {
    poll_rate = 10,
    ignore_done_already = true,
    ignore_empty_message = true,
    display = {
      format_annote = function(msg) return msg.title end,
      progress_ttl = 30,
    },
    -- 'ltex' sends a progress update for *every single keystroke*...
    ignore = { "ltex" },
    lsp = {
      -- See: https://github.com/j-hui/fidget.nvim/issues/167
      progress_ringbuf_size = 2048,
    },
  },
  notification = {
    override_vim_notify = false,
    view = {
      stack_upwards = false,
    },
    window = {
      winblend = 100,
      border = "none",
      max_width = 70,
      normal_hl = "Comment",
      y_padding = 1
    },
  },

}
