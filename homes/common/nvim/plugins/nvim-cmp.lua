local cmp = require("cmp")
cmp.setup {
    snippet = {
        expand = function(args)
            require("luasnip").lsp_expand(args.body)
        end,
    },
    window = {
        documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert {
        -- confirm completion item
        ["<CR>"] = cmp.mapping.confirm({ select = false }),
        -- toggle completion menu
        ["<C-e>"] = cmp.mapping.abort(),
        -- tab complete
        ["<Tab>"] = cmp.mapping.select_next_item(),
        ["<S-Tab>"] = cmp.mapping.select_prev_item(),
        -- scroll documentation window
        ["<C-f>"] = cmp.mapping.scroll_docs(-5),
        ["<C-d>"] = cmp.mapping.scroll_docs(5),
    },
    sources = cmp.config.sources({
        { name = "nvim_lsp" },
        { name = "luasnip", keyword_length = 2 },
        { name = "nvim_lua" },
        -- {
        --   -- See https://github.com/f3fora/cmp-spell
        --   name = "spell",
        --   option = {
        --     keep_all_entries = false,
        --     enable_in_context = function() return true end,
        --   }
        -- }
    }, {
        { name = "greek" },
        { name = "path" },
        { name = "buffer", keyword_length = 3 },
    }),
    preselect = "item",
    formatting = {},
    completion = {
        completeopt = "menu,menuone,noinsert",
    },
}
