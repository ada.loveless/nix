require("nvim-tree").setup {
    view = {
        width = 40,
    },
    filters = {
        dotfiles = true,
    }
}
require("which-key").add{
    { "<leader>tt",  "<Cmd>:NvimTreeToggle<CR>", desc="Toggle Tree" },
    { "<leader>tr",  "<Cmd>:NvimTreeRefresh<CR>", desc="Refresh Tree" },
}
