require("luasnip.loaders.from_lua").load { paths = "~/.config/nvim/LuaSnip/" }
require("luasnip.loaders.from_vscode").lazy_load()
require("luasnip").config.setup {
    enable_autosnippets = true,
}
require("which-key").add({
    { "<C-Tab>", "<Plug>luasnip-jump-next", desc="Jump to next insert" },
}, { mode = { "i", "s" } })
