require("onedark").setup {
    style = "warmer",
    transparent = true,
    term_colors = true,
    ending_tildes = false,
    lualine = {
        transparent = false,
    },
    diagnostics = {
        darker = true,
        undercurl = true,
        background = true,
    },
    colors = {
        bg0 = "#333333",
        bg1 = "#393939",
        bg2 = "#383838",
        bg3 = "#3a3a3a",
        bg_d = "#2c2c2c",
        fg = "#f4f4f4",
        red = "#de4d68",
        dark_red = "#a34141",
        green = "#8ad454",
        yellow = "#ead858",
        blue = "#57a5e5",
        purple = "#bb70d2",
        dark_purple = "#79428a",
        cyan = "#51a8b3",
        dark_cyan = "#328f96",
        orange = "#ee9542",
        light_grey = "#848484",
        grey = "#777777",
        diff_delete = "#5e4040",
        diff_change = "#2d4a61",
        diff_add = "#313c28",
    },
}
vim.opt.background = "dark"
vim.opt.termguicolors = true
vim.cmd("colo onedark")
