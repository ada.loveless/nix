require("gitsigns").setup {
    signs = {
        add = { text = "▎" },
        change = { text = "▎" },
        delete = { text = "" },
        topdelete = { text = "" },
        changedelete = { text = "▎" },
        untracked = { text = "▎" },
    },
}
require("which-key").add {
        { "<leader>hS",  "<Cmd>Gitsigns stage_buffer<CR>", desc="Stage buffer" },
        { "<leader>hu",  "<Cmd>Gitsigns undo_stage_hunk<CR>", desc="Undo stage" },
        { "<leader>hR",  "<Cmd>Gitsigns reset_buffer<CR>", desc="Reset buffer" },
        { "<leader>hp",  "<Cmd>Gitsigns preview_hunk<CR>", desc="Preview hunk" },
        { "<leader>hb",  "<Cmd>Gitsigns blame_line full=true<CR>", desc="Blame line" },
        { "<leader>hd",  "<Cmd>Gitsigns diffthis<CR>", desc="Diff this" },
        { "<leader>hD",  "<Cmd>Gitsigns diffthis ~<CR>", desc="Diff this" },
        { "<leader>hs",  ":Gitsigns stage_hunk<CR>:w<CR>", desc="Stage hunk", mode = { "n", "v" } },
}
require("which-key").add {
    { "]h", ":Gitsigns next_hunk<CR>", desc="Next hunk", mode = { "n", "v" } },
    { "[h", ":Gitsigns prev_hunk<CR>", desc="Previous hunk", mode = { "n", "v" } },
    { "ih", "<Cmd>:<C-U>Gitsigns select_hunk<CR>", desc="Select hunk", mode = { "o", "x" } },
}
