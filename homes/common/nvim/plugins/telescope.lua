require("which-key").add{
    { "<leader>ff",  "<Cmd>:Telescope find_files<CR>", desc="Find Files" },
    { "<leader>fg",  "<Cmd>:Telescope live_grep<CR>", desc="Live Grep" },
    { "<leader>fb",  "<Cmd>:Telescope buffers<CR>", desc="Buffers" },
    { "<leader>fh",  "<Cmd>:Telescope help_tags<CR>", desc="Search Help" },
    { "<leader>fH",  "<Cmd>:Telescope keymaps<CR>", desc="Search Keymaps" },
    { "<leader>fd",  "<Cmd>:Telescope diagnostics<CR>", desc="Search Diagnostics" },
    { "<leader>fD",  "<Cmd>:Telescope lsp_document_symbols<CR>", desc="Search Document Symbols" },
    { "<leader>ft",  "<Cmd>:Telescope treesitter<CR>", desc="Search Treesitter Symbols" },
    { "<leader>fr",  "<Cmd>:Telescope restore<CR>", desc="Restore last Telescope" },
    { "<leader>fy",  "<Cmd>:Telescope registers<CR>", desc="Search Registers" },
    { "<leader>GC",  "<Cmd>:Telescope git_commits<CR>", desc="Search Git Commits" },
    { "<leader>GS",  "<Cmd>:Telescope git_status<CR>", desc="Search Git Status" },
}
require("which-key").add {
    { "gr",  "<Cmd>:Telescope lsp_references<CR>", desc="LSP references" },
}
require("telescope").setup {
  pickers = {
    find_files = {
      hidden = true,
      -- no_ignore = true,
    },
  },
}
