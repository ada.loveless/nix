require("dressing").setup {
    input = {
        enabled = true,
        title_pos = "left",
        border = "rounded",
        buf_options = {},
        win_options = {
            wrap = false,
            list = true,
        },
    },
    select = {
        backend = "telescope",
    },
    builtin = {
        relative = "win",
    }
}
