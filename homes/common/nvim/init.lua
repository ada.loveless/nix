-- generic options
vim.opt.nu = true
vim.opt.rnu = true
vim.opt.numberwidth = 4
vim.opt.mouse = "a"
vim.opt.laststatus = 2
vim.opt.autowrite = true
vim.opt.timeout = true
vim.opt.timeoutlen = 500
vim.opt.cursorline = true
vim.opt.list = true
vim.defer_fn(
-- Hideous workaround becauset this doesn't work normally in newer nvim
  function()
    vim.opt.listchars = {
      tab = "▸ ",
      eol = "↵",
      leadmultispace = "│   ",
      -- trail = "╳",
      nbsp = "⍽",
    }
  end,
  100
)

vim.opt.et = true
vim.opt.sw = 4
vim.opt.ts = 4

-- Split to the right and below
vim.opt.splitbelow = true
vim.opt.splitright = true

-- Ruler at column 81
vim.opt.colorcolumn = "81,101"

vim.api.nvim_command("autocmd TermOpen * setlocal nonu")  -- no numbers
vim.api.nvim_command("autocmd TermOpen * setlocal nornu") -- no numbers

-- prevent typo when pressing `wq` or `q`
vim.cmd [[
cnoreabbrev <expr> W ((getcmdtype() is# ":" && getcmdline() is# "W")?("w"):("W"))
cnoreabbrev <expr> Q ((getcmdtype() is# ":" && getcmdline() is# "Q")?("q"):("Q"))
cnoreabbrev <expr> WQ ((getcmdtype() is# ":" && getcmdline() is# "WQ")?("wq"):("WQ"))
cnoreabbrev <expr> Wq ((getcmdtype() is# ":" && getcmdline() is# "Wq")?("wq"):("Wq"))
]]

-- Leader is space
vim.g.mapleader = " "
vim.g.maplocalleader = " "

vim.api.nvim_create_autocmd("LspAttach", {
  desc = "Language server actions",
  callback = function(event)
    local wk = require("which-key")
    local opts = { buffer = event.buf }
    wk.add({
      { "gd", "<cmd>lua vim.lsp.buf.definition()<cr>",      desc = "Jump to definition",      opts },
      { "gD", "<cmd>lua vim.lsp.buf.declaration()<cr>",     desc = "Jump to declaration",     opts },
      { "gi", "<cmd>lua vim.lsp.buf.implementation()<cr>",  desc = "List implementations",    opts },
      { "go", "<cmd>lua vim.lsp.buf.type_definition()<cr>", desc = "Jump to type definition", opts },
      { "gR", "<cmd>lua vim.lsp.buf.references()<cr>",      desc = "List references",         opts },
      { "gs", "<cmd>lua vim.lsp.buf.signature_help()<cr>",  desc = "Signature information",   opts },
      { "gl", "<cmd>lua vim.diagnostic.open_float()<cr>",   desc = "Floating diagnostics",    opts },
      { "K",  "<cmd>lua vim.lsp.buf.hover()<cr>",           desc = "Show function signature", opts },
      { "]d", "<cmd>lua vim.diagnostic.goto_next()<cr>",    desc = "Goto next diagnostic",    opts },
      { "[d", "<cmd>lua vim.diagnostic.goto_prev()<cr>",    desc = "Goto prev diagnostic",    opts },
    }, { mode = "n", })
    wk.add({
      { "<F2>", "<cmd>lua vim.lsp.buf.rename()<cr>",               desc = "Format buffer", opts },
      { "<F3>", "<cmd>lua vim.lsp.buf.format({async = true})<cr>", desc = "Format buffer", opts },
      { "<F4>", "<cmd>lua vim.lsp.buf.code_action()<cr>",          desc = "Code action",   opts },
    }, { mode = { "n", "x", "v" } })
  end,
})
require("which-key").add({
  { "<leader>df",  "<Cmd>lua vim.diagnostic.open_float()<CR>", desc = "float" },
  { "<leader>ds",  "<Cmd>lua vim.diagnostic.show()<CR>",       desc = "show" },
  { "<leader>dh",  "<Cmd>lua vim.diagnostic.hide()<CR>",       desc = "hide" },
  { "<leader>dd1", function() Set_diag_severity(1) end,        desc = "error" },
  { "<leader>dd2", function() Set_diag_severity(2) end,        desc = "warning" },
  { "<leader>dd3", function() Set_diag_severity(3) end,        desc = "info" },
  { "<leader>dd4", function() Set_diag_severity(4) end,        desc = "hint" },
  {
    "<leader>dq",
    function()
      vim.diagnostic.setqflist({ severity = vim.b.diag_severity or nil })
    end,
    desc = "to quickfix"
  },
  {
    "<leader>dp",
    function()
      vim.diagnostic.goto_prev({ severity = vim.b.diag_severity or nil })
    end,
    desc = "previous"
  },
  {
    "<leader>dn",
    function()
      vim.diagnostic.goto_next({ severity = vim.b.diag_severity or nil })
    end,
    desc = "next"
  },

}, { mode = { "n" } })
vim.diagnostic.config {
  underline = false,
  virtual_text = true,
  severity_sort = true,
  float = {
    style = "minimal",
    border = "rounded",
    source = "always",
    header = "",
    prefix = "",
  },
}
require("notify").setup {
  stages = "static",
  background_colour = "#000000",
}
require("which-key").add({
  { "<leader>Ny", "<Cmd>Notifications<CR>",                   desc = "history (plain)" },
  {
    "<leader>Nh",
    function()
      local telescope = require("telescope")
      telescope.load_extension("notify")
      telescope.extensions.notify.notify()
    end,
    desc = "history"
  },
  { "<leader>Nd", function() require("notify").dismiss() end, desc = "dismiss" },
  { "<leader>Np", function() require("notify").pending() end, desc = "show pending" },
}, { mode = { "n" } })
vim.notify = require("notify")
vim.b.diag_severity = { min = 4, max = 1 }
function Set_diag_severity(severity)
  vim.b.diag_severity = { min = severity, max = 1 }

  vim.diagnostic.config({
    virtual_text = {
      severity = vim.b.diag_severity,
      source = "if_many",
      spacing = 1,
    },
    signs = {
      severity = vim.b.diag_severity,
    },
    update_in_insert = false,
    severity_sort = true,
  })
end

vim.opt.completeopt = { "menu", "menuone", "noselect" }
vim.o.timeout = true
vim.o.timeoutlen = 300

vim.g.vimtex_compiler_method = "tectonic"


-- Folding
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "v:lua.vim.treesitter.foldexpr()"
vim.opt.foldopen = "hor,mark,percent,quickfix,search,tag"
vim.opt.foldcolumn = "auto:4"
vim.opt.foldlevelstart = 0
vim.opt.foldminlines = 1
vim.opt.foldtext = ""
-- vim.opt.fillchars:append({ fold = "<" })
