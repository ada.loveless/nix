{ config, pkgs, ... }:

{
  imports = [ 
    ./spell.nix
  ];

  home.packages = with pkgs; [
    nvimpager
  ];

  home.sessionVariables = {
    EDITOR = "nvim";
    VISUAL = "nvim";
  };

  xdg.configFile = {
    "nvim/ftdetect/containerfile.vim".text = ''
      au BufRead,BufNewFile *.containerfile set filetype=dockerfile
      au BufRead,BufNewFile *.Containerfile set filetype=dockerfile
    '';
    "nvim/ftplugin/tex.lua".source = ./tex.lua;
    "nvim/ftplugin/nix.lua".source = ./nix.lua;
    "nvim/ftplugin/openscad.lua".text = ''
      vim.opt.commentstring = "// %s"
    '';
  };

  programs.neovim = {
    enable = true;
    defaultEditor = true;
    package = pkgs.unstable.neovim-unwrapped;
    viAlias = true;
    vimAlias = true;
    vimdiffAlias = true;
    withNodeJs = true;
    extraLuaConfig = builtins.readFile ./init.lua;
    plugins = with pkgs.vimPlugins; [
      vim-sensible
      which-key-nvim
      # style
      nvim-web-devicons
      {
        plugin = onedark-nvim;
        type = "lua";
        config = builtins.readFile ./plugins/onedark.lua;
      }
      {
        plugin = lualine-nvim;
        type = "lua";
        config = builtins.readFile ./plugins/lualine.lua;
      }
      # git
      {
        plugin = gitsigns-nvim;
        type = "lua";
        config = builtins.readFile ./plugins/gitsigns.lua;
      }
      vim-fugitive
      # language server
      friendly-snippets
      {
        plugin = luasnip;
        type = "lua";
        config = builtins.readFile ./plugins/luasnip.lua;
      }
      {
        plugin = nvim-lspconfig;
        type = "lua";
        config = builtins.readFile ./plugins/lspconfig.lua;
      }
      {
        plugin = lsp_signature-nvim;
        type = "lua";
        config = builtins.readFile ./plugins/lsp_signature.lua;
      }
      cmp-nvim-lsp
      cmp-nvim-lua
      cmp-buffer
      cmp-greek
      # cmp-spell
      cmp_luasnip
      {
        plugin = nvim-cmp;
        type = "lua";
        config = builtins.readFile ./plugins/nvim-cmp.lua;
      }
      {
        plugin = fidget-nvim;
        type = "lua";
        config = builtins.readFile ./plugins/fidget.lua;
      }
      nvim-notify
      # treesitter
      {
        plugin = nvim-treesitter;
        type = "lua";
        config = builtins.readFile ./plugins/treesitter.lua;
      }
      {
        plugin = comment-nvim;
        type = "lua";
        config = builtins.readFile ./plugins/comment.lua;
      }
      # Treesitter parsers
      ## Languages I use 
      nvim-treesitter-parsers.lua
      nvim-treesitter-parsers.latex
      nvim-treesitter-parsers.typst
      nvim-treesitter-parsers.sql
      nvim-treesitter-parsers.julia
      nvim-treesitter-parsers.python
      nvim-treesitter-parsers.rust
      nvim-treesitter-parsers.dockerfile
      nvim-treesitter-parsers.nix
      nvim-treesitter-parsers.bash
      nvim-treesitter-parsers.graphql
      ## Languages I don't use
      nvim-treesitter-parsers.scheme
      nvim-treesitter-parsers.c
      ## Misc
      nvim-treesitter-parsers.markdown
      nvim-treesitter-parsers.markdown_inline
      nvim-treesitter-parsers.make
      nvim-treesitter-parsers.ebnf
      ## Serialisation formats
      nvim-treesitter-parsers.xml
      nvim-treesitter-parsers.html
      nvim-treesitter-parsers.yaml
      nvim-treesitter-parsers.json
      nvim-treesitter-parsers.jq
      nvim-treesitter-parsers.kdl
      nvim-treesitter-parsers.ini
      ## Misc other files
      nvim-treesitter-parsers.vim
      nvim-treesitter-parsers.vimdoc
      nvim-treesitter-parsers.git_rebase
      # file browsing
      {
        plugin = nvim-tree-lua;
        type = "lua";
        config = builtins.readFile ./plugins/nvim-tree.lua;
      }
      plenary-nvim
      {
        plugin = telescope-nvim;
        type = "lua";
        config = builtins.readFile ./plugins/telescope.lua;
      }
      {
        plugin = dressing-nvim;
        type = "lua";
        config = builtins.readFile ./plugins/dressing.lua;
      }
      {
        plugin = wilder-nvim;
        type = "lua";
        config = builtins.readFile ./plugins/wilder.lua;
      }
      {
        plugin = diffview-nvim;
        type = "lua";
        config = builtins.readFile ./plugins/diffview.lua;
      }
      # File format-specific things
      vimtex
      typst-vim
      julia-vim
      mediawiki-vim
      {
        plugin = vim-table-mode;
        type = "lua";
        # config = ''
        #   require("which-key").register({
        #     "tm" ={"<Cmd>:TableModeToggle<CR>",    desc = "Toggle Table Mode" },
        #   }, {prefix = "<leader>"})
        # '';
      }
      mini-icons
    ];
    extraPackages = with pkgs; [
      # LSPs
      texlab
      nil
      unstable.tinymist
      unstable.typstyle
      nixpkgs-fmt
      lua-language-server
      yaml-language-server
      nodePackages.bash-language-server
      openscad-lsp
      # dependencies of other things (like treesitter)
      gcc
      cargo
    ];
  };
}
