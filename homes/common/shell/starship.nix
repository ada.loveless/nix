{ config, lib, pkgs, ... }:

{
  programs.starship = {
    enable = true;
    enableZshIntegration = true;
    settings = {
      # Prompt-wide config
      format = lib.concatStrings [
        "$sudo"
        "$username"
        "$hostname"
        "$directory"
        "$direnv"
        "$git_branch"
        # "$git_commit"
        # "$git_state"
        "$git_status"
        "$git_metrics"
        "$container"
        # "$julia"
        "$package"
        "$status"
        "$character"
      ];
      right_format = lib.concatStrings [
        "$battery"
        "$time"
      ];
      add_newline = false;
      # Modules
      sudo = {
        symbol = "wizard ";
        style = "bright-purple";
        format = "[$symbol]($style)";
        disabled = false;
      };
      username = {
        show_always = true;
        style_user = "purple";
        style_root = "bold red";
        format = "[$user]($style) ";
      };
      hostname = {
        ssh_only = true;
        format = "on [$hostname]($style) ";
      };
      directory = {
        truncation_length = 3;
        truncate_to_repo = false;
        style = "green";
        format = "in [$path]($style) ";
      };
      git_branch = {
        style = "cyan";
        format = "on [$symbol$branch(:$remote_branch)]($style)";
      };
      git_status = {
        style = "bold cyan";
        format = "([ <$all_status$ahead_behind>]($style)) ";
        deleted = "x";
        ahead = "^";
        behind = "v";
        diverged = "-";
        renamed = "r";
      };
      git_metrics = {
        disabled = true;
      };
      container = {
        format = "⬢";
        style = "";
      };
      direnv = {
        disabled = false;
        format = "[direnv $loaded/$allowed]($style) ";
        # symbol = "direnv ";
        style = "blue";
      };
      package = {
        symbol = "";
      };
      status = {
        disabled = false;
        format = "[$status](red)";
      };
      character = let char = "%"; in {
        success_symbol = "${char}";
        error_symbol = "[${char}](red)";
        vimcmd_symbol = "[${char}](bold green)";
        vimcmd_replace_one_symbol = "[${char}](bold purple)";
        vimcmd_replace_symbol = "[${char}](bold purple)";
        vimcmd_visual_symbol = "[${char}](bold yellow)";
      };
      battery = {
        full_symbol = "=";
        charging_symbol = "^";
        discharging_symbol = "v";
        format = "[$percentage($symbol)]($style)";
      };
      time = {
        format = " [$time]($style)";
        style = "white";
        disabled = false;
      };
    };
  };
}
