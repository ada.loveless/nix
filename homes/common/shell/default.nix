{ config, lib, pkgs, ... }:

let
  info-vim-cfg = pkgs.stdenvNoCC.mkDerivation {
    name = "info-vim-cfg";
    src = pkgs.fetchFromGitLab {
      owner = "hartang";
      repo = "dotfiles";
      rev = "1cec0da986f8b12e1604e22bb50f2815675ab6be";
      hash = "sha256-38uBQET7B7nICgW79+aK9BJwy9hdRYMQvl4nOwTnCzM=";
    };
    installPhase = ''
      mkdir "$out"
      cp .infokey "$out/.infokey"
    '';
  };
  hartan-scripts = pkgs.stdenvNoCC.mkDerivation {
    name = "hartan-scripts";
    src = pkgs.fetchFromGitLab {
      owner = "c8160";
      repo = "shell-helpers";
      rev = "67d000d7291efbedda90178e598f81d7080df93c";
      hash = "sha256-sg45/qHn9lpTaaHipK3qFWP5Oywo4A4JNWCq9ygVJ8w=";
    };
    nativeBuildInputs = with pkgs; [
      gnused
    ];
    buildInputs = with pkgs; [
      bash
    ];
    patchPhase = ''
      sed -i 's/source "$HOME\/.magic.bash"/source magic.bash/' whatsup
      sed -i 's/source "$HOME\/.magic.bash"/source magic.bash/' gitlab-sync
    '';
    installPhase = ''
      mkdir -p "$out/bin"
      cp magic.bash "$out/bin/magic.bash"
      cp gitlab-merge "$out/bin/gitlab-merge"
      cp gitlab-sync "$out/bin/gitlab-sync"
      cp whatsup "$out/bin/whatsup"
    '';
  };
in
{
  imports = [
    ./starship.nix
  ];

  programs = {
    zsh = {
      enable = true;
      autosuggestion.enable = true;
      enableCompletion = true;
      defaultKeymap = "viins";
      initExtraFirst = builtins.readFile ./zshrc.pre;
      # Read zshrc.post and do sort of aliases, but cooler
      initExtra = (builtins.readFile ./zshrc.post) + ''
        # Open a train on bahn.expert
        train-nr() {
          ${pkgs.xdg-utils}/bin/xdg-open "https://bahn.expert/details/$1";
        }
        # Open a train station on bahn.expert
        bahnhof() {
          ${pkgs.xdg-utils}/bin/xdg-open "https://bahn.expert/$1";
        }
        # Open a train station on bahnhof.de
        bahnhof-de() {
          NONCE=91473b6e8dc173127f972248841c01067e64df55
          RESPONSE=$(${pkgs.curl}/bin/curl "https://www.bahnhof.de/suche" \
            -sS \
            -X POST \
            -H "Next-Action: $NONCE" \
            -H "Content-Type: text/plain;charset=UTF-8" \
            --data-raw "[\"$1\"]")
          SLUG="$(echo "$RESPONSE" | tail -1 | cut -d':' -f2- | ${pkgs.jq}/bin/jq -er '.[0].slug')"
          ${pkgs.xdg-utils}/bin/xdg-open "https://www.bahnhof.de/$SLUG"
        }
      '';
      shellAliases = {
        rebuild = "sudo echo -n && sudo nixos-rebuild switch |& nom";
        wormhole = "wormhole-rs";
      };
    };

    git = {
      aliases = {
        c = "commit";
        ca = "commit --amend";
        cc = "rev-parse --short=8 HEAD";
        d = "diff";
        dc = "diff --cached";
        p = "push";
        pf = "push --force-with-lease";
        pF = "push --force";
        br = "branch";
        co = "checkout";
        st = "status";
        lb = "for-each-ref --sort=-committerdate refs/heads/ --format='%(align:width=2,position=right)%(*upstream:trackshort)%(end) %(color: italic 220)%(committerdate:short) %(color: reset)%(refname:short)'";
        lbr = "for-each-ref --sort=-committerdate refs/remotes/ --format='%(color: italic 220)%(committerdate:short) %(color: reset)%(refname:short)'";
        wm = "!${pkgs.glab}/bin/glab mr view -w";
        wc = "!${pkgs.xdg-utils}/bin/xdg-open $(${pkgs.glab}/bin/glab mr show --output json | ${pkgs.jq}/bin/jq -er '.head_pipeline.web_url')";
        wr = "!${pkgs.glab}/bin/glab repo view -w";
      };
    };

    nushell = {
      enable = true;
      package = pkgs.unstable.nushell;
    };

    zoxide = {
      enable = true;
      enableZshIntegration = true;
      options = [
        "--cmd cd"
      ];
    };

    direnv = {
      enable = true;
      nix-direnv.enable = true;
    };
  };

  home.file.".infokey" = {
    enable = true;
    source = "${info-vim-cfg}/.infokey";
  };


  home.packages = [
    hartan-scripts
  ];

}
