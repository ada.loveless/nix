{ config, pkgs, ... }:

{
  services.gpg-agent = {
    enable = true;
    enableBashIntegration = true;
    enableZshIntegration = true;
    enableScDaemon = true;
    grabKeyboardAndMouse = true;
    enableSshSupport = true;
    pinentryPackage = pkgs.pinentry-qt;
    extraConfig = ''
      use-standard-socket
    '';
  };
}
