# Non-graphical common user-home config
{ config, lib, pkgs, ... }:

{
  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.11"; # Please read the comment before changing.

  imports = [
    ./nvim
    ./shell
    ./zellij
    ./lf.nix
    ./services.nix
    ./emacs.nix
  ];

  home.sessionPath = [
    "$HOME/.local/bin"
  ];

  home.packages = with pkgs; [
    # tools
    xdg-utils
    chafa
    ghostscript
    yubikey-manager
    exiftool
    qrencode
    wormhole-rs
    poppler_utils
    toot
    hunspell
    hunspellDicts.de_DE
    bc
    # languages
    rustup
    guile
    gauche
    julia
    hunspell
    hunspellDicts.de_DE
    hunspellDicts.en_GB-ise
    # fonts
    crimson
    b612
    google-fonts
    atkinson-hyperlegible
    fira-code-nerdfont
  ];

  xdg.mimeApps.enable = true;
  # The defaultApplications have been placed in graphical/default.nix as they are all graphical applications.

  systemd.user.tmpfiles.rules = let home = config.home.homeDirectory; in [
    # Autoclean ~/tmp
    "d ${home}/tmp - - - 3d -"
  ];

  targets.genericLinux.enable = true;

  programs = {
    fzf = {
      enable = true;
    };
    yt-dlp.enable = true;
    info.enable = true;
    btop = {
      enable = true;
      settings = {
        # color_theme = "";
        theme_background = false;
        vim_keys = true;
        rounded_corners = false;
        graph_symbol = "block";
        proc_gradient = false;
      };
    };
    gpg = {
      enable = true;
      scdaemonSettings = {
        disable-ccid = true;
        reader-port = "Yubico Yubikey";
      };
    };
    git = {
      enable = true;
      lfs.enable = true;
      extraConfig = {
        init.defaultBranch = "main";
        merge.tool = "nvimdiff2";
        diff.tool = "nvimdiff";
        mergetool.keepBackup = false;
        push = {
          autoSetupRemote = true;
          default = "current";
        };
      };
    };
  };
}
