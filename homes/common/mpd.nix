{ config, pkgs, lib, ... }:

{

  home.packages = with pkgs; [
    cantata
    mpc-cli
  ];

  services.mopidy = {
    enable = true;
    extensionPackages = with pkgs; [
      mopidy-mpd
      mopidy-tidal
      mopidy-local
      mopidy-notify
    ];
    settings = {
      audio = {
        mixer = "none";
        output = "pulsesink";
      };
      mpd = {
        enabled = true;
        hostname = "::";
        port = 6600;
      };
      local = {
        enabled = true;
        media_dir = "~/Music";
      };
      file.enabled = false;
      notify.enabled = true;
      tidal = {
        enabled = false;
        quality = "LOSSLESS";
      };
    };
  };

  programs.ncmpcpp = {
    enable = true;
    mpdMusicDir = "~/Music";
  };

}
