{ config, lib, pkgs, ... }:

{
  programs.zellij = {
    enable = true;
    enableZshIntegration = true; # This auto-starts zellij on zsh startup
    settings = {
      session_serialization = false;
      simplified_ui = true;
      rounded_corners = false;
    };
  };
  xdg.configFile = {
    "zellij/layouts/monitor.kdl".source = ./monitor.kdl;
    "zellij/layouts/nixos.kdl".source = ./nixos.kdl;
  };
}
