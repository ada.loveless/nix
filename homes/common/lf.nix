{ config, lib, pkgs, ... }:

{
  programs.lf = {
    enable = true;
    settings = {
      shellopts = "-eu";
      relativenumber = true;
      tabstop = 4;
      sixel = true;
      mouse = true;
    };
    previewer.source = with pkgs; writeShellScript "pv.sh" ''
      #!/bin/sh
      case "$1" in
        *.tar.gz|*.tgz) ${gnutar}/bin/tar tzf "$1" ;;
        *.tar.bz2|*.tbz2) ${gnutar}/bin/tar tjf "$1" ;;
        *.tar) ${gnutar}/bin/tar tf "$1" ;;
        *.zip|*.jar) ${unzip}/bin/unzip -l"$1" ;;
        *)
          case "$(file -Lb --mime-type -- "$1")" in 
            image/*)
              ${chafa}/bin/chafa -f sixel -s "$2x$3" --animate off --polite on "$1"
            ;;
            text/*)
              ${bat}/bin/bat --paging=never --color=always --decorations=always --style=numbers "$1"
            ;;
            *)
              ${bat}/bin/bat --paging=never --color=always --decorations=always --style=numbers "$1"
            ;;
          esac
        ;;
      esac
    '';
  };
}
