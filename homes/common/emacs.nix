{ config, lib, pkgs, ... }:

{
  programs.emacs = {
    enable = false;
    extraConfig = ''
      (custom-set-variables
       '(custom-enabled-themes '(tango-dark))
       '(scroll-bar-mode nil)
       '(tool-bar-mode nil))
    '';
    # extraConfig = ''
    #   (require 'evil)
    #   (evil-mode 1)
    #   (require 'evil-leader)
    #   (global-evil-leader-mode)
    #   (evil-leader/set-leader "<SPC>")
    # '';
    package = pkgs.emacs-gtk;
    # extraPackages = epkgs: with epkgs; [
    #   evil
    #   evil-org
    #   evil-leader
    #   undo-tree
    # ];
  };
}
