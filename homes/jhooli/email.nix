{ config, pkgs, ... }:

{
  accounts.email.accounts.sulzmann = let cfg = config.accounts.email.accounts.sulzmann; in {
    primary = true;
    thunderbird = {
      enable = true;
      perIdentitySettings = id: {
        "mail.identity.id_${id}.compose_html" = false;
        "mail.identity.id_${id}.reply_on_top" = 1;
      };
    };
    realName = "Jooa Hooli";
    address = "jooa.hooli@sulzmann.energy";
    userName = "${cfg.address}";
    imap = {
      host = "imap.ionos.de";
      port = 993;
      tls = {
        enable = true;
        useStartTls = false;
      };
    };
    smtp = {
      host = "smtp.ionos.de";
      port = 465;
      tls = {
        enable = true;
        useStartTls = false;
      };
    };
    signature = {
      text = ''
        ${cfg.realName} 
      '';
      showSignature = "append";
    };
  };
}
