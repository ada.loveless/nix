{ config, pkgs, ... }:

{
  imports = [
    ./sway.nix
    ./zellij
    ./bin
  ];

  home.username = "jhooli";
  home.homeDirectory = "/home/jhooli";
  programs.git = {
    userEmail = "jooa.hooli@sulzmann.energy";
    userName = "Jooa Hooli";
  };
  home.packages = with pkgs; [
    gnumake
    parquet-tools
    typst-dev
    # clockodo-cli
    glab
  ];
  xdg.configFile."typst/pkg-mirror.toml" = {
    enable = true;
    text = ''
      [sulzmann]
      path = "https://gitlab.sulzmann.energy/api/v4/projects/35/packages/generic/$name/$version/$name-$version.tar.gz"
      index = "https://gitlab.sulzmann.energy/api/v4/projects/35/packages"
    '';
  };
}
