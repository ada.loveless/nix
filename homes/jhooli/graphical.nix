{ config, pkgs, ... }:

{
  imports = [
    ./default.nix
    ./email.nix
    ./firefox.nix
  ];

  services = {
    nextcloud-client.enable = true;
    nextcloud-client.startInBackground = true;
  };

  home.packages = with pkgs; [
    tidal-hifi
    strawberry
    libreoffice
  ];

  programs.vscode = {
    enable = false;
    extensions = with pkgs.vscode-extensions; [
      reditorsupport.r
    ];
    userSettings = {
      # "r.alwaysUseActiveTerminal" = true;
      # "r.bracketedPaste" = true;
      # "r.sessionWatcher" = true;
      "editor.fontFamily"= "Atkinson Hyperlegible Mono";
      "editor.fontLigatures"= true;
      "debug.console.fontFamily"= "Atkinson Hyperlegible Mono";
      "debug.console.fontSize"= 10;
      "editor.fontSize"= 10;
      "terminal.integrated.fontSize" = 10;
    };
    enableUpdateCheck = false;
    enableExtensionUpdateCheck = false;
  };

  wayland.windowManager.sway.config.output."*".bg = "~/.background.png fill";
  wayland.windowManager.sway.checkConfig = false;

  home.file.".background.png" = {
    enable = true;
    source = ../graphical/backgrounds/background-bad.png;
    target = ".background.png";
  };
}
