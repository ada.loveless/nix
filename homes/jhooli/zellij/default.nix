{ config, lib, pkgs, ... }:

{
  xdg.configFile."zellij/layouts/enos.kdl".source = ./enos.kdl;
  xdg.configFile."zellij/layouts/report.kdl".source = ./report.kdl;
}
