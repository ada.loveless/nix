{ config, pkgs, lib, ... }:

{
  programs.firefox.profiles.default = {
    bookmarks = [
      {
        name = "work";
        toolbar = true;
        bookmarks = map (x: x // { tags = [ "work" ]; }) [
          { name = "clockodo"; url = "https://my.clockodo.com/"; tags = [ "time-tracking" ]; }
          { name = "nextcloud"; url = "https://cloud.sulzmann.energy/"; tags = [ "organisation" ]; }
          { name = "webmail"; url = "https://email.ionos.de/"; tags = [ "e-mail" ]; }
          { name = "gitlab"; url = "https://gitlab.sulzmann.energy/"; tags = [ "code" ]; }
          { name = "enos"; url = "https://enos.sulzmann.energy/"; tags = [ "code" ]; }
          { name = "r-search"; url = "https://search.r-project.org/"; tags = [ "cran" "r" ]; }
          { name = "pypi"; url = "https://pypi.org/"; tags = [ "python" ]; }
          { name = "crates.io"; url = "https://crates.io/"; tags = [ "rust" ]; }
          { name = "typst Reference"; url = "https://typst.app/docs/reference/"; tags = [ "typst" ]; }
        ];
      }
    ];
  };
}
