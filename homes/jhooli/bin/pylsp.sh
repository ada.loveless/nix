#!/usr/bin/env bash
set -euo pipefail

# source "$(dirname "$(realpath "$0")")/enos_env.sh"

# Container tag to execute (default: "Latest"). Set the environment variable
# 'ENOS_CONTAINER_PYTHON_TAG' to overwrite it.
TAG="${ENOS_CONTAINER_PYTHON_TAG:-latest}"
IMAGE="${ENOS_CONTAINER_PYTHON_IMAGE:-"registry.sulzmann.energy/work/enos/enos-backend/python"}"
CONTAINER="$IMAGE:$TAG"

if ! command -v podman &>/dev/null; then
    echo "'podman' is required to run this command" 1>&2
    exit 1
fi

declare -a PODMAN_ARGS=(
    --rm -i
    --security-opt "label=disable"
    --log-driver "none"
    # Disables dumping pycache files all over
    -e "PYTHONDONTWRITEBYTECODE"
    -e "ENOS_*"
    -v "$PWD:$PWD" -w "$(realpath "$PWD")"
    --pull "newer"
    --entrypoint '["pylsp"]'
)

podman run "${PODMAN_ARGS[@]}" "$CONTAINER" "$@"
