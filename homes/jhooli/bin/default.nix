{ config, pkgs, lib, ... }:
let
  pylsp = pkgs.writeShellApplication {
    name = "pylsp";
    runtimeInputs = with pkgs; [
      coreutils
      podman
      bash
    ];
    text = builtins.readFile ./pylsp.sh;
  };
in
{
  home.packages = [
    pylsp
  ];
}
