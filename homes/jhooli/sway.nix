{ config, pkgs, ... }:

{
  wayland.windowManager.sway.extraConfig = ''
    workspace 9
    exec ${pkgs.thunderbird}/bin/thunderbird
    workspace 6
    exec ${pkgs.firefox}/bin/firefox
    workspace 10
    exec ${pkgs.foot}/bin/foot ${pkgs.zellij}/bin/zellij -l monitor
    workspace 1
    exec_always kanshictl reload
  '';
}
